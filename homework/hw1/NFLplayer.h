/** Copyright 2014 Barry C. Harris Jr. */

#include <iostream>
#include <string>

#ifndef NFLplayer_H
#define NFLplayer_H

/**
 * Struct to hold relevant data for an NFLplayer
 */
struct Shotcaller {
 public:
        Shotcaller();

        std::string school, league, name, position, team;
};
#endif
