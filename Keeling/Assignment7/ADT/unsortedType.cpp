#include "unsortedType.h"

nodeType::nodeType() {
    this->next = NULL;
    this->info.clientID = 0;
    this->info.clientGender = '\0';
    this->info.firstName = "";
    this->info.lastName = "";
    this->info.Address = "";
    this->info.City = "";
    this->info.State = "";
    this->info.Zip = 0;
}

nodeType::~nodeType() {
    delete next;
}

unsortedType::unsortedType() {
    this->head_ = NULL;
}

unsortedType::~unsortedType() {
    nodeType * temp = new nodeType();
    while (temp != NULL) {
        temp = head_->next;
        delete head_;
        head = temp;
    }
}

void unsortedType::add_item(clientType & item) {
    nodeType * temp = new nodeType();
    temp->info = item;
    if (head_ == NULL) {
        head_ = temp;
    } else {
        temp->next = head_;
        head_ = temp;
    }
}

void unsortedType::print() {
    if (head_ == NULL) {
        std::cout << "Empty list" << std::endl << std::endl;
    } else if (head_->next == NULL) {
        head_->info.print_Clients();
    } else {
        nodeType * traverse = new nodeType();
        traverse = head_;
        while (traverse != NULL) {
            traverse->info.print_Clients();
            traverse = traverse->next;
        }
        delete traverse;
    }
}
