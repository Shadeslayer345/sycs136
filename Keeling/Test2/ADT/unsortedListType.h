#include <cstddef>

#include "studentType.h"

#ifndef KEELING_TEST2_ADT_NODETYPE_H
#define KEELING_TEST2_ADT_NODETYPE_H
class nodeType {
 public:
    nodeType();
    ~nodeType();

    studentType info;
    nodeType * next;
};
#endif

#ifndef KEELING_TEST2_ADT_UNSORTEDLISTTYPE_H
#define KEELING_TEST2_ADT_UNSORTEDLISTTYPE_H
class unsortedListType {
 public:
    unsortedListType();
    ~unsortedListType();

    void insertMid(studentType & new_student);
    void insertEnd(studentType & new_student);
    void insertFront(studentType & new_student);
    void printList();

    nodeType * firstPtr;
    int length;
};
#endif
