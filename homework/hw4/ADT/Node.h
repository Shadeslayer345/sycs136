/** Copyright 2014 Barry C. Harris Jr. **/
#include <iostream>

#ifndef HOMEWORK_HW4_ADT_Node
#define HOMEWORK_HW4_ADT_Node
class Node {
 public:
    void show_data();
    int data() const;
    Node* next();

    void set_data(int data);
    void set_next(Node *next);
 private:
    int data_;
    Node* next_;
};  // end Node class declaration
#endif
