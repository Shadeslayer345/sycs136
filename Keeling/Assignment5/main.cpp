#include "ADT/clientType.h"

#include <fstream>
#include <iostream>

interestType find_interest(std::string test_string);

void print_clients(clientType clients[]);

int main()
{
    clientType members[20];
    std::string i1, i2;
    std::ifstream myfile;
    myfile.open("infile.txt");
    for( int i = 0; i < 6; i++) {

        myfile >> members[i].lastName >> members[i].firstName >> 
            members[i].personNum >> members[i].client_ID >> 
            members[i].AddressNum >> members[i].Address >> 
            members[i].AddressDec >> members[i].City >> members[i].State >>
            members[i].Zip >> members[i].gender;

        myfile >> i1 >> i2;

        members[i].client_interest[0] = find_interest(i1);
        members[i].client_interest[1] = find_interest(i2);
    }

    print_clients(members);
    myfile.close();
    return 0;
}

interestType find_interest(std::string test_string) {
    if (test_string == "yoga") {
        return yoga;
    } else if (test_string == "family") {
        return family;
    } else if (test_string == "roadtrips") {
        return roadtrips;
    } else if (test_string == "birdwatching") {
        return birdwatching;
    } else if (test_string == "cooking") {
        return cooking;
    } else if (test_string == "backpacking") {
        return backpacking;
    } else if (test_string == "hiking") {
        return hiking;
    } else if (test_string == "twitter") {
        return twitter;
    } else if (test_string == "facebook") {
        return facebook;
    } else if (test_string == "photography") {
        return photography;
    } else if (test_string == "travel") {
        return travel;
    } else if (test_string == "romance") {
        return romance;
    } else if (test_string == "TV") {
        return TV;
    } else if (test_string == "technology") {
        return technology;
    } else if (test_string == "reading") {
        return reading;
    } else if (test_string == "dining") {
        return dining;
    } else if (test_string == "running") {
        return running;
    } else if (test_string == "picnics") {
        return picnics;
    } else if (test_string == "movies") {
        return movies;
    } else if (test_string == "theater") {
        return theater;
    } else {
        return sports;
    }
}

std::string print_interest(interestType interest) {
    switch(interest) {
        case 0: return "sports";
        case 1: return "theater";
        case 2: return "movies";
        case 3: return "picnics";
        case 4: return "running";
        case 5: return "dining";
        case 6: return "reading";
        case 7: return "technology";
        case 8: return "TV";
        case 9: return "romance";
        case 10: return "travel";
        case 11: return "photography";
        case 12: return "facebook";
        case 13: return "twitter";
        case 14: return "hiking";
        case 15: return "backpacking";
        case 16: return "cooking";
        case 17: return "backpacking";
        case 18: return "roadtrips";
        case 19: return "family";
        case 20: return "yoga";
    }
}

void print_clients(clientType clients[]) {
    for (int i = 0; i < 6; ++i)
    {
        std::cout << "Name: " << clients[i].firstName << " " <<
            clients[i].lastName << std::endl <<
            "Person Number: " << clients[i].personNum << std::endl <<
            "Client ID: " << clients[i].client_ID << std::endl <<
            "Gender: " << clients[i].gender << std::endl <<
            "Address: " << clients[i].AddressNum << " " <<
            clients[i].Address << " " << clients[i].AddressDec <<
            clients[i].City << " " << clients[i].State << " " <<
            clients[i].Zip << std::endl << "Interests: " <<
            print_interest(clients[i].client_interest[0]) << ", " <<
            print_interest(clients[i].client_interest[1]) <<
            std::endl << std::endl;
    }
}

/**
    Name: Jill Herold,
    Person Number: 1
    Client ID: 2234
    Gender: F
    Address: 123 Main St.Washington, DC 20019
    Interests: yoga, facebook

    Name: Stan Jackson,
    Person Number: 2
    Client ID: 3748
    Gender: M
    Address: 12 Douglas Ave.Baltimore, MD 30229
    Interests: sports, movies

    Name: Francis Jerry,
    Person Number: 3
    Client ID: 6666
    Gender: M
    Address: 2345 6th StreetWoodbridge, VA 44040
    Interests: movies, roadtrips

    Name: Wilson Joan,
    Person Number: 4
    Client ID: 1234
    Gender: F
    Address: 12 Georgia Ave.Washington, DC 20019
    Interests: romance, dining

    Name: Stanley Smith,
    Person Number: 5
    Client ID: 3456
    Gender: M
    Address: 56 D StreetBaltimore, MD 30229
    Interests: movies, dining

    Name: Claude Claire,
    Person Number: 6
    Client ID: 2311
    Gender: F
    Address: 66 32nd StreetWoodbridge, VA 44040
    Interests: cooking, facebook
*/
