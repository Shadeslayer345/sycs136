/** Copyright 2014 Barry C. Harris Jr. **/
#include "Node.h"

/**
 * Prints data for NFL player.
 */
void Node::show_data() {
    data_.print();
}

NFL Node::data() const {
    return data_;
}

/**
 * Retrieves the next node in the linked list.
 * @return [description]
 */
Node* Node::next() {
    return next_;
}

/**
 * Sets data for new node.
 * @param data NFL object that will serve as the data in the node.
 */
void Node::set_data(NFL& data) {
    data_ = data;
}

/**
 * Sets the next element in memory for the linked.
 * @param next The next node in the linked list or NULL.
 */
void Node::set_next(Node *next) {
    next_ = next;
}

