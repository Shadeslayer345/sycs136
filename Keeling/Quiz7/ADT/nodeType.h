#include "routeType.h"

#ifndef KEELING_QUIZ7_ADT_NODETYPE_H
#define KEELING_QUIZ7_ADT_NODETYPE_H

typedef routeType itemType;

struct nodeType {
    itemType info;
    nodeType* next;
};
#endif
