#include "personType.h"

enum interestType {sports, theater, movies, 
    picnics, running, dining, reading, technology, TV, romance, travel,
    photography, facebook, twitter, hiking, backpacking,
    cooking, birdwatching, roadtrips, family, yoga};

#ifndef CLIENTTYPE
#define CLIENTTYPE
class clientType : public personType
{
public:
    clientType();
    std::string client_ID;
    interestType client_interest[10];
};
#endif
