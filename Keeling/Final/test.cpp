#include <fstream>
#include <iostream>
#include <string>

struct PlantType {
 public:
    std::string plantName;
    float plantWeight;
};

class NodeType {
 public:
    NodeType();
    ~NodeType();
    PlantType info;
    NodeType * next;
};

NodeType::NodeType() {
    this->info.plantName = "";
    this->info.plantWeight = 0.0;
    this->next = NULL;
}

NodeType::~NodeType() {
}

class LinkedListType {
 public:
    LinkedListType();
    ~LinkedListType();
    void insert(PlantType new_plant);
    void print();
 private:
    NodeType * firstptr;
};

LinkedListType::LinkedListType() {
    this->firstptr = NULL;
}

LinkedListType::~LinkedListType() {
    NodeType * traverse = firstptr;

    while (traverse != NULL) {
        NodeType * temp = traverse;
        traverse = traverse->next;
        delete temp;
    }
}

void LinkedListType::insert(PlantType new_plant) {
    NodeType * temp = new NodeType();
    temp->info = new_plant;

    if (firstptr == NULL || firstptr->info.plantWeight >= temp->info.plantWeight) {
        temp->next = firstptr;
        firstptr = temp;
    } else {
        float current_weight = temp->info.plantWeight;
        NodeType * traverse = firstptr;

        while (traverse->next != NULL &&
                    traverse->next->info.plantWeight < current_weight) {
            traverse = traverse->next;
        }

        temp->next = traverse->next;
        traverse->next = temp;
    }
}

void LinkedListType::print() {
    NodeType * temp = firstptr;

    while (temp != NULL) {
        std::cout << temp->info.plantName << " " << temp->info.plantWeight <<
                std::endl;
        temp = temp->next;
    }
}


int main(int argc, char const *argv[])
{
    std::ifstream myfile;
    myfile.open("infile.txt");
    std::string plantNames[28];
    std::string plantWeights[28];
    LinkedListType plantList;
    for (int i = 0; i < 28; ++i) {
        std::string first, last;
        myfile >> first >> last;
        plantNames[i] = first + " " + last;
    } for (int i = 0; i < 28; ++i) {
        myfile >> plantWeights[i];
    } for (int i = 0; i < 28; ++i) {
        PlantType temp_plant;
        temp_plant.plantName = plantNames[i];
        temp_plant.plantWeight = std::stof(plantWeights[i]);
        plantList.insert(temp_plant);
    }
    plantList.print();
    return 0;
}
