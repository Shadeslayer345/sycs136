#include <fstream>
#include <iostream>
#include <string>

// Header File

#ifndef KEELING_QUIZ6_ADT_CONTACTTYPE_H
#define KEELING_QUIZ6_ADT_CONTACTTYPE_H
class contactType {
 public:
    contactType();
    contactType(std::string new_first_name, std::string new_last_name);
    ~contactType();
    std::string first_name;
    std::string last_name;
    std::string *phone_number_ptr;
};
#endif

// Implementation File

contactType::contactType() {
    first_name = "";
    last_name = "";
    phone_number_ptr = NULL;
}

contactType::contactType(std::string new_first_name, std::string new_last_name) {
    first_name = new_first_name;
    last_name = new_last_name;
    phone_number_ptr = NULL;
}

contactType::~contactType() {
    first_name = "";
    last_name = "";
    delete phone_number_ptr;
}

// Driver File

void print(contactType* mycontacts[]);

int main() {
    std::ifstream infile;
    infile.open("infile.txt");
    contactType *contacts[20];
    int count = 0;
    std::string first_name, last_name, phone;
    infile >> first_name >> last_name >> phone;
    while(infile) {
        contacts[count] = new contactType(first_name, last_name);
        infile >> first_name >> last_name >> phone;
        std::string *phone_ptr = new std::string();
        *phone_ptr = phone;
        contacts[count]->phone_number_ptr = phone_ptr;
        count++;
    }
    print(contacts);
    return 0;
}

void print(contactType* mycontacts[]) {
    for(int i = 0; i < 5; i++) {
        std::cout << "Name: " << mycontacts[i]->first_name << " " <<
                mycontacts[i]->last_name << std::endl <<
               "Phone #: " << *(mycontacts[i]->phone_number_ptr) << std::endl <<
               std::endl;
    }
}

/**
 *
➜  Quiz6 git:(master) ✗ clang++ combined.cpp 
➜  Quiz6 git:(master) ✗ ./a.out
Name: Benjamin Henderson
Phone #: 102-345-9999

Name: Cornell Jameson
Phone #: 102-543-8888

Name: Dolly Dickinson
Phone #: 123-456-7777

Name: Kenny Ken
Phone #: 896-999-1344

Name: Vicky Ick
Phone #: 896-999-1344
 */
