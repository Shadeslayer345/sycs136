#include <fstream>
#include <iostream>
#include <string>

#include "ADT/contactType.h"

void print(contactType* mycontacts[]);

int main() {
    std::ifstream infile;
    infile.open("infile.txt");
    contactType *contacts[20];
    int count = 0;
    std::string first_name, last_name, phone;
    infile >> first_name >> last_name >> phone;
    while(infile) {
        contacts[count] = new contactType(first_name, last_name);
        infile >> first_name >> last_name >> phone;
        std::string *phone_ptr = new std::string();
        *phone_ptr = phone;
        contacts[count]->phone_number_ptr = phone_ptr;
        count++;
    }
    print(contacts);
    return 0;
}

void print(contactType* mycontacts[]) {
    for(int i = 0; i < 5; i++) {
        std::cout << "Name: " << mycontacts[i]->first_name << " " <<
                mycontacts[i]->last_name << std::endl <<
               "Phone #: " << *(mycontacts[i]->phone_number_ptr) << std::endl <<
               std::endl;
    }
}
