/** Copyright 2014 Barry C. Harris Jr. **/ 
#include "../NFL.h"

#ifndef HOMEWORK_HW3_ADT_Node
#define HOMEWORK_HW3_ADT_Node
class Node {
 public:
    void show_data();
    NFL data() const;
    Node* next();

    void set_data(NFL& data);
    void set_next(Node *next);
 private:
    NFL data_;
    Node* next_;
};  // end Node class declaration
#endif
