#include "ExtMoneyType.h"

ExtMoneyType::ExtMoneyType(long dollars, long cents, const std::string currency) 
                            : MoneyType(dollars, cents) {
    currency_ = currency;
}

std::string ExtMoneyType::CurrencyIs() const {
    return currency_;
}
