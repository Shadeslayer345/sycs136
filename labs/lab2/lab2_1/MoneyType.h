#include <string>

#ifndef MONEYTYPE_H
#define MONEYTYPE_H

class MoneyType
{
 public:
    MoneyType();
    MoneyType(long, long);
    long DollarsAre() const;
    long CentsAre() const;

 private:
    long dollars_, cents_;
};
#endif
