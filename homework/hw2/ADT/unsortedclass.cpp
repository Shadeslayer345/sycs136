/** Copyright 2014 Barry C. Harris Jr. */

#include "unsortedclass.h"

/**
 * Default constructor, starts length at zero.
 */
unsortedclass::unsortedclass() {
    length_ = 0;
}

/**
 * Adds a player to the list
 * @param new_player A NBA class object.
 */
void unsortedclass::add_player(NBA& new_player) {
    if (length_ < (MAX_CLASS_LENGTH)) {
        NBA_players[length_] = new_player;
    } else {
        NBA_players[MAX_CLASS_LENGTH-1] = new_player;
    }
    length_++;
}

/**
 * Returns the number of NBA objects in the list.
 * @return Length of the array.
 */
int unsortedclass::length() const {
    return length_;
}

/**
 * Tests for equivalancy between two NBA objects.
 * @param  player1 NBA object.
 * @param  player2 NBA object.
 * @return The outcome, zero is they are equal and 1 if not.
 */
int unsortedclass::Compare(NBA player1, NBA player2) {
    if ((player1.get_first_initial() == player2.get_first_initial()) &&
            (player1.get_last_name().compare(player2.get_last_name())) &&
            (player1.get_position() == player2.get_position()) &&
            (player1.get_team().compare(player2.get_team()))) {
            
        return 0;
    } else {
        return 1;
    }
}

/**
 * Tests to see if an object being inserted is already present, this tests for
 *     the only constant value between the objects: name as no two players 
 *     should have the same name.
 * @param  first_initial
 * @param  name
 * @return Outcome, zero if not found and one if found.
 */
int unsortedclass::is_original(char first_initial, std::string last_name) {
    for (int i = 0; i < length_; ++i) {
        NBA current_player = NBA_players[i];
        if (last_name.compare(current_player.get_last_name()) &&
                first_initial == current_player.get_first_initial()) {
            return 0;
        }
    }
    return 1;
}

/**
 * Retrieves a player from the array, assuming that each player has a unique
 *     name.
 * @param  first_initial
 * @param  last_name
 * @return An NBA object from the list that matches the query.
 */
NBA unsortedclass::get_player_by_name(char first_initial, std::string last_name) {
    for (int i = 0; i < length_; ++i) {
        NBA current_player = NBA_players[i];
        if (last_name.compare(current_player.get_last_name()) == 0 &&
            first_initial == current_player.get_first_initial()) {
            return current_player;
        }
    }

    // If player not found empty player will be returned
    std::cout << "Name not found! Retruning empty player instead!" << std::endl;
    NBA empty_player;
    return empty_player;
}
