/** Copyright 2014 Barry C. Harris Jr. **/

#include <iostream>

#ifndef LABS_LAB7_ADT_STACKTYPE_H
#define LABS_LAB7_ADT_STACKTYPE_H

class FullStack {};
class EmptyStack {};

int const MAX_ITEMS = 10;

class StackType {
 public:
    StackType(); // Constructor

    bool IsEmpty() const;
    bool IsFull() const;
    void Pop();
    void Push(char item);
    char Top() const;
 private:
    int top;
    char items[MAX_ITEMS];
};
#endif
