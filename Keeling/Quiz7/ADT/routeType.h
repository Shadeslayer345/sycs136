#include <string>
#ifndef KEELING_TEST1_ADT_ROUTETYPE_H
#define KEELING_TEST1_ADT_ROUTETYPE_H
class routeType {
 public:
    routeType();
    std::string from_city, to_city;
    int capacity, current_passenger_count;
};
#endif
