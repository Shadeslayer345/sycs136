#include <string>

#ifndef EMPLOYEE_H
#define EMPLOYEE_H
class Employee {
 public:
 	Employee();
    Employee(std::string full_name, int id);
 	Employee(std::string full_name, int id, int payrate);
 	
 	int id();
 	int payrate();
 	std::string full_name();
 	int total_pay(int hours_worked);
 	
 	void set_id(int id);
 	void set_full_name(std::string full_name);
 	void set_payrate(int payrate);
 private:
	int id_, payrate_;
	std::string full_name_;
};
#endif  //End of Employee class definition
