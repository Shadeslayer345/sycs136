#include <iostream>

#include "ADT/Quetype.h"

bool identical(QueType& que1, QueType que2);

int main() {

    QueType que1;
    QueType que2;

    que1.Enque('b');
    que1.Enque('a');
    que1.Enque('r');
    que1.Enque('r');
    que1.Enque('y');
    
    que2.Enque('b');
    que2.Enque('a');
    que2.Enque('r');
    que2.Enque('r');
    que2.Enque('y');

    bool check = identical(que1, que2);

    std::cout << "Que 1 & Que 2 are identical: " << check <<
            std::endl;

    return 0;
}

bool identical(QueType& que1, QueType que2) {
    if (!(que1.IsEmpty() && que2.IsEmpty())) {
        if (que1.Front() == que2.Front()) {
            que1.Deque();
            que2.Deque();
            identical(que1, que2);
        } else {
            std::cout << "Values not equal" << std::endl;
            return false;
        }
    } else if (que1.IsEmpty() != que2.IsEmpty()) {
        return false;
    } return true;
}

