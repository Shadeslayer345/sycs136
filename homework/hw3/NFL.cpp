/** Copyright 2014 Barry C. Harris Jr. */
#include "NFL.h"

/**
 * Default constructor, sets the player's league to NFL.
 */
NFL::NFL() {
    league_ = "NFL";
}

std::string NFL::first_name() const {
    return first_name_;
};

std::string NFL::last_name() const {
    return last_name_;
};

std::string NFL::league() const {
    return league_;
}

std::string NFL::position() const {
    return position_;
};

std::string NFL::school() const {
    return school_;
};

std::string NFL::team() const {
    return team_;
};

void NFL::set_team(std::string team) {
    team_= team ;
};

void NFL::set_first_name(std::string first_name) {
   first_name_ = first_name;
};

void NFL::set_last_name(std::string last_name) {
    last_name_ = last_name;
};

void NFL::set_position(std::string position) {
    position_ = position;
};

void NFL::set_school(std::string school) {
    school_ = school ;
};

void NFL::print() {
    std::cout << "Name: " << first_name_ << " " << last_name_ << std::endl <<
    "Current Team: " << team_ << std::endl <<
    "Position: " << position_ << std::endl <<
    "School: " << school_ << std::endl;
}
