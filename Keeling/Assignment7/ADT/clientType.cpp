#include "clientType.h"

std::string convert_interest_to_string(interestType interest);

clientType::clientType() {
    clientID = 0;
    clientGender = ' ';
}

void clientType::set_FirstName(std::string name) {
    firstName = name;
}

void clientType::set_LastName(std::string name) {
    lastName = name;
}

void clientType::set_Address (std::string s1, std::string s2, std::string s3) {
    Address = s1 + " " + s2 + " " + s3;
}

void clientType::set_City (std::string s) {
    City = s;
}

void clientType::set_State(std::string s) {
    State = s;
}

void clientType::set_Zip(int s) {
    Zip = s;
}

void clientType::set_clientID(int idnumber) {
    clientID = idnumber;
}

void clientType::set_clientGender(char genderletter) {
    clientGender = genderletter;
}

int clientType::get_clientID() {
    return clientID;
}

char clientType::get_clientGender() {
    return clientGender;
}

interestType clientType::get_clientInterests() {
    return clientInterests[0];
}

void clientType::print_Clients() {
    std::cout << clientID << " "<< firstName << " " << lastName << std::endl
    << Address << std::endl << City << " " << State << " " << Zip << std::endl
    << clientGender << std::endl
    << convert_interest_to_string(clientInterests[0]) << " "
    << convert_interest_to_string(clientInterests[1]) << std::endl << std::endl;
}

std::string convert_interest_to_string(interestType interest) {
    switch (interest) {
        case sports:
                return "sports";
                break;
        case theater:
                return "theater";
                break;
        case movies:
                return "movies";
                break;
        case picnics:
                return "picnics";
                break;
        case running:
                return "running";
                break;
        case dining:
                return "dining";
                break;
        case reading:
                return "reading";
                break;
        case technology:
                return "technology";
                break;
        case TV:
                return "TV";
                break;
        case romance:
                return "romance";
                break;
        case travel:
                return "travel";
                break;
        case photography:
                return "photography";
                break;
        case facebook:
                return "facebook";
                break;
        case twitter:
                return "twitter";
                break;
        case hiking:
                return "hiking";
                break;
        case backpacking:
                return "backpacking";
                break;
        case cooking:
                return "cooking";
                break;
        case birdwatching:
                return "birdwatching";
                break;
        case roadtrips:
                return "roadtrips";
                break;
        case family:
                return "family";
                break;
        case yoga:
                return "yoga";
                break;
        default:
                return "unknown";
       }
}
