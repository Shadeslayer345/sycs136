/** Copyright 2014 Barry C. Harris Jr. */

#include "NFL.h"

/**
 * Default constructor for an NFL player which sets it's league data member.
 * @constructor
 */
NFL::NFL() {
    league = "NFL";
}

NFL::NFL(std::string new_school, std::string new_name, std::string new_position, std::string new_team) {
	league = "NFL";
	school = new_school;
	name = new_name;
	position = new_position;
	team = new_team;
}
