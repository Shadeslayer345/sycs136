#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
using namespace std;

class StudentType
{
public:
    StudentType();
    bool operator < (const StudentType& s);
    bool operator > (const StudentType& s);
    bool operator == (const StudentType& s);
    bool operator != (const StudentType& s);
    int teamID;
    string firstName, lastName;
    string city;
};

bool StudentType::operator< (const StudentType& s)
{
    if(lastName > s.lastName)
        return false;
    else if(lastName < s.lastName)
        return true;
    else if (lastName == s.lastName)
    {
        if(firstName <= s.firstName)
            return true;
        else
            return false;
    }
    else
        return false;
}

bool StudentType::operator== (const StudentType& s)
{
    if(lastName == s.lastName && firstName == s.firstName)
        return true;
    else
        return false;
}

bool StudentType::operator!= (const StudentType& s)
{
    if(s.lastName != lastName || s.firstName != firstName)
        return true;
    else
        return false;
}

bool StudentType::operator> (const StudentType& s)
{
    if(lastName < s.lastName)
        return false;
    else if (lastName > s.lastName)
        return true;
    else if (lastName == s.lastName)
    {
        if(firstName >= s.firstName)
            return true;
        else
            return false;
    }
    else
        return false;
}

StudentType::StudentType()
{
    firstName = "";
    lastName = "";
    teamID = -1;
    city = "";
}
struct NodeType
{
    StudentType info;
    NodeType* next;
    NodeType* prev;
    NodeType();
};

NodeType::NodeType()
{
    next = NULL;
    prev = NULL;
}

class SortedListType {
    public:
        SortedListType();
        SortedListType(const SortedListType& origin);
        ~SortedListType();
        bool IsEmpty() const;
        bool IsFull();
        int GetLength() const;
        NodeType* Front();
        NodeType* Back();
        NodeType* GetItem(NodeType* search, int low, int high);
        NodeType* GoToNode(NodeType* current, int move, char direc);
        void Insert(NodeType* node);
        void InsertMid(NodeType* node);
        void Print();

    private:
        NodeType* first;
        NodeType* front;
        NodeType* back;
        NodeType* current;
        int length;
};

SortedListType::SortedListType() {
    first = NULL;
    front = NULL;
    back = NULL;
    current = NULL;
    length = 0;
}

SortedListType::SortedListType(const SortedListType& src) {
    NodeType * traverse;
    NodeType * last;
    if (src.first == NULL) {
        first = NULL;
    } else {
        first = new NodeType();
        first->info = src.first->info;

        traverse = src.first->next;
        last = first;

        for (int i = 0; i < src.length; ++i) {
            last->next = new NodeType();
            last = last->next;
            last->info = traverse->info;
            traverse = traverse->next;
        }
        last->next = NULL;
    }
    length = src.length;
    front = first;
}

SortedListType::~SortedListType() {
    NodeType* temp;
    first->prev = NULL;
    back->next = NULL;
    while(!IsEmpty()) {
        temp = first;
        first = first->next;
        delete temp;
    }
}

bool SortedListType::IsEmpty() const {
    if(first == NULL)
        return true;
    else
        return false;
}

bool SortedListType::IsFull() {
    NodeType* temp;
    try
    {
        temp = new NodeType;
        delete temp;
        return false;
    }
    catch(bad_alloc ba)
    {
        return true;
    }
}

int SortedListType::GetLength() const {
    return length;
}

NodeType* SortedListType::Front() {
    return front;
}

NodeType* SortedListType::Back() {
    return back;
}

NodeType* SortedListType::GetItem(NodeType* search, int low, int high) {
    int mid = (low+high)/2;
    if(low > high)
        return current;
    else if (search->info==current->info)
        return current;
    else if (search->info < GoToNode(current, mid, 'b')->info)
    {
        current=GoToNode(current, mid, 'f');
        return GetItem(search, mid-1, high);
    }
    else
    {
        current=GoToNode(current, mid, 'f');
        return GetItem(search, low, mid+1);
    }
}

NodeType* SortedListType::GoToNode(NodeType* current, int move, char direc) {
    current = front;
    for(int i = 0; i < length/2; i++)
        current = current->next;

    if(direc == 'b' || 'B')
    {
        for(int i = 0; i < move; i++)
            current = current->prev;
        return current;
    }
    else if (direc == 'f' || 'F')
    {
        for(int i = 0; i < move; i++)
            current = current->next;
        return current;
    }
    else
    {
        printf("Invalid direction selected\n\n");
        return current;
    }
}

void SortedListType::Insert(NodeType* node)
{
    if(first == NULL)
    {
        first = new NodeType;
        first->info = node->info;
        front = first;
        back = front;
        front->next = back;
        back->prev = front;
        front->prev = back;
        back->next = front;
        length++;
    }
    else
    {
        NodeType* location;
        current = front;
        while(true)
        {
            //Special Case Insert Front
            if(node->info < front->info)
            {
                location = new NodeType;
                location->info = node->info;
                location->next = front;
                location->prev = back;
                front->prev = location;
                front = location;
                back->next = front;
                first = front;
                length++;
                break;
            }
            //General Case Insert within list
            if (node->info < current->info)
            {
                NodeType* temp;
                temp = new NodeType;
                temp->info = node->info;
                location->next = temp;
                temp->next = current;
                temp->prev = location;
                current->prev = temp;
                length++;
                break;
            }

            //Special Case Insert Back
            if(node->info > back->info)
            {
                NodeType* temp;
                temp = new NodeType;
                temp->info = node->info;
                back->next = temp;
                back = back->next;
                back->next = front;
                front->prev = back;
                length++;
                break;
            }
            location = current;
            current = current->next;
        }
    }
}

void SortedListType::InsertMid(NodeType* node)
{
    NodeType* tempLoc = new NodeType;
    NodeType* location;
    current = front;
    tempLoc->info = node->info;

    for(int i = 0; i < length/2; i++)
    {
        location = current;
        current = current->next;
    }

    location->next = tempLoc;
    tempLoc->prev = location;
    tempLoc->next = current;
    current->prev = tempLoc;
}

void SortedListType::Print()
{
    current = front;
    for(int i = 0; i < length; i++)
    {
        printf("%s, %s\n", current->info.lastName.c_str(), current->info.firstName.c_str());
        current = current->next;
    }
}

void heading();

int main() {
    //Declare variables
    heading();
    ifstream infile;
    NodeType * data = new NodeType;
    NodeType * temp = new NodeType;
    SortedListType studentList;
    //Read data into sorted list
    infile.open("infile.txt");
    while(infile.good()) {
        infile>>data->info.firstName >> data->info.lastName;
        infile>>data->info.teamID >> data->info.city;
        studentList.Insert(data);
    }
    //Print list
    studentList.Print();

    SortedListType newstudent_list = studentList;

    std::cout << std::endl << std::endl << "New list" << std::endl;

    newstudent_list.Print();

    return 0;
}

void heading()
{
    printf("Howard Student\n");
    printf("@02600000\n");
    printf("SYCS-136 Computer Science II\n");
    printf("April 16\n");
    printf("Quiz 14\n\n");
}
