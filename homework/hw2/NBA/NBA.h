/** Copyright 2014 Barry C. Harris Jr. */

#include <iostream>
#include <string>

#ifndef NBA_H
#define NBA_H

/**
 * Class to handle creation and addition of NBA player data.
 */
class NBA {
 public:
        NBA();
        NBA(char first_initial, std::string last_name, char position, std::string team);

        std::string get_last_name() const;
        char get_first_initial() const;
        char get_position() const;
        std::string get_team() const;

        void set_last_name(std::string name);
        void set_first_initial(char initial);
        void set_position(char position);
        void set_team(std::string team);
        void display_NBA();

 private:
        std::string league_, last_name_, team_;
        char first_initial_, position_;
};
#endif  // NBA_H
