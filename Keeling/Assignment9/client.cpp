#include "ADT/time.h"

int main() {
    Time T1(12, 30);
    std::cout << "The first time is " << T1 << std::endl;
    Time T2(14, 30);
    Time T3(14, 30);
    Time T4(1, 45);
    Time T5;
    Time T6(2, 50);

    std::cout << "The second time is " << T2 << std::endl;

    if (T1 > T2) {
        std::cout << T1 << " is later than " << T2 << std::endl;
    }
    else {
        std::cout << T2 << " is later than " << T1 << std::endl;
    }

    std::cout << "The third time is " << T3 << std::endl;

    if (T2 == T3) {
        std::cout << T2 << " is equal to " << T3 << std::endl;
    }
    else {
        std::cout << T2 << " is not equal to " << T3 << std::endl;
    }

    if (T2 == T1) {
        std::cout << T2 << " is equal to " << T1 << std::endl;
    }
    else {
        std::cout << T2 << " is not equal to " << T1 << std::endl;
    }

    T5 = T4 + T3;
    std::cout << T5 << " = " << T4 << " + " << T3 << std::endl;
    T3 = T5 - T4;
    std::cout << T3 << " = " << T5 << " - " << T4 << std::endl;
    T4 = T6 * 2;
    std::cout << T4 << " = " << T6 << " * " << 2 << std::endl;
    return 0;
}
