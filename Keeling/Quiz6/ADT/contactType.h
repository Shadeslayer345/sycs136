#include <string>

#ifndef KEELING_QUIZ6_ADT_CONTACTTYPE_H
#define KEELING_QUIZ6_ADT_CONTACTTYPE_H
class contactType {
 public:
    contactType();
    contactType(std::string new_first_name, std::string new_last_name);
    ~contactType();
    std::string first_name;
    std::string last_name;
    std::string *phone_number_ptr;
};
#endif
