/** Copyright 2014 Barry C. Harris Jr. */

#include "NBA.h"

/**
 * Default constructor for the NBAplayer class which explicitly sets the league
 * data member.
 * @constructor
 */
NBA::NBA() {
    league_ = "NBA";
}

/**
 * Constructor for the NBAplayer class which allows an instance to be created
 * with all data members assigned.
 * @constructor
 */
NBA::NBA(char first_initial, std::string last_name, char position, std::string team) {
    first_initial_ = first_initial;
    league_ = "NBA";
    last_name_ = last_name;
    position_ = position;
    team_ = team;
}

char NBA::get_first_initial() const {
	return first_initial_;
}

/**
 * Returns The value of the name data member.
 * @return The name value
 */
std::string NBA::get_last_name() const {
    return last_name_;
}

/**
 * Retuns The value of the position data member.
 * @return The position value
 */
char NBA::get_position() const {
    return position_;
}

/**
 * Returns The value of the team data member.
 * @return The team value
 */
std::string NBA::get_team() const {
    return team_;
}

void NBA::set_first_initial(char first_initial) {
	first_initial_ = first_initial;
}

/**
 * Changes the value of the last name data member.
 * @param  name The new name value.
 */
void  NBA::set_last_name(std::string last_name) {
    last_name_ = last_name;
}

/**
 * Changes the value of the position data member.
 * @param  position The new position value.
 */
void  NBA::set_position(char position) {
    position_ = position;
}

/**
 * Changes the value of the team data member.
 * @param  team The new team value
 */
void  NBA::set_team(std::string team) {
    team_ = team;
}

/**
 * Prints all data members of the NBA.
 */
void NBA::display_NBA() {
    std::cout << "League: NBA" << std::endl <<
            "(1) Name: " << first_initial_ << ". "<< last_name_ << std::endl <<
            "(2) Current Team: " << team_ << std::endl <<
            "(3) Position: " << position_ << std::endl;
}
