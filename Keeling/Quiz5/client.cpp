#include <iostream>

#include "ADT/personType.h"

int main() {
    // Question 1
    personType Barry;
    // Question 2
    personType *meptr;
    // Question 3
    meptr = &Barry;
    // Question 4
    personType *youptr;
    // Question 5
    youptr = new personType();
    // Question 6
    youptr->name = "Barry";
    return 0;
}
