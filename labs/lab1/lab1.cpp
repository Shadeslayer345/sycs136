#include <iostream>
#include <fstream>

int main() {
    std::ofstream hello;
    std::ifstream days_input;
    std::ofstream days_output;
    std::string input;

    std::cout << "Enter an integer ======> ";

    int days_in_semester = 0;
    std::cin >> days_in_semester;
    days_output.open("days.txt");
    days_output << days_in_semester;
    days_output.close();


    days_input.open("days.txt");
    if (days_input.is_open())
    {
        getline(days_input, input);
    }
    days_input.close();

    hello.open("hello.txt");
    hello << "Hello world! Only " << input << " days til the " <<
                "end of the semester!" << std::endl;
    hello.close();

    return 0;
}
