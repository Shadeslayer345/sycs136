/** Copyright 2014 Barry C. Harris Jr. **/
#include "Stack.h"

Stack::Stack() { // Default Constructor
    top = NULL;
}

bool Stack::IsEmpty() const {
    return (top == NULL);
}

bool Stack::IsFull() const {
    Node* newNode;
    try {
        newNode = new Node();
        delete newNode;
        return false;
    } catch (std::bad_alloc exception) {
        return true;
    }
}

void Stack::Pop() {
    if (IsEmpty()) {
        throw EmptyStack();
    } else {
        Node* temp;
        temp = top;
        top = top->next();
        delete temp;
    }
}

void Stack::Push(int newItem) {
    if (IsFull()) {
        throw FullStack();
    } else {
        Node* newNode;
        newNode = new Node();
        newNode->set_data(newItem);
        newNode->set_next(top);
        top = newNode;
    }
}

Node* Stack::Top() {
    if (IsEmpty()) {
        throw EmptyStack();
    } else {
        return top;
    }
}
