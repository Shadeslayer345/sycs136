#ifndef CLASSWORK_36PRES_QUETYPE_H
#define CLASSWORK_36PRES_QUETYPE_H

class FullQue {};
class EmptyQue {};

class QueType {
 public:
    QueType();
    QueType(int max);

    bool IsEmpty() const;
    bool IsFull() const;
    int Front() const;
    int Rear()  const;

    void MakeEmpty();
    void Enque(char item);
    void Deque();
 private:
    char* items;
    int front, rear;
    int maxQue;
};
#endif
