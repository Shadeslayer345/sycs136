#include <iostream>

#include "ADT/Stack.h"

void sort(Stack& start, Stack& middle, Stack& end);

/**
 * Client code to take 5 unique input variables display them in order of
 *     insertion and sort them using only linked based stacks.
 * @return 0
 */
int main() {
    Stack stack1; // Will be used as starting point for all data entry
    Stack stack2;
    Stack stack3; // Final stack that will be in sorted order

    /** Initial input of unique values **/
    int input = 0;

    for (int i = 0; i < 5; i++) {
        std::cin >> input;
        if (stack1.IsEmpty()) {
            stack1.Push(input);
        } else if (stack1.Top()->data() != input) {
            stack1.Push(input);
        }
    }

    /** Outputting the order of insertion using second stack **/
    std::cout << "Here's the stack you entered entered: ";
    for (int i = 0; i < 5; i++) {
        stack2.Push(stack1.Top()->data());
        stack1.Pop();
    }
    for (int i = 0; i < 5; i++) {
        std::cout << stack2.Top()->data() << " ";
        stack1.Push(stack2.Top()->data());
        stack2.Pop();
    } std::cout << std::endl;

    /** Call to sort stack using all three stacks **/
    sort(stack1, stack2, stack3);

    /** Output of final stack with correct ordering **/
    std::cout << "Here's the sorted stack (Descending): ";
    for (int i = 0; i < 5; i++) {
        std::cout << stack3.Top()->data() << " ";
        stack3.Pop();
    } std::cout << std::endl;
    return 0;
}

/**
 * Used to order the elements of a stack in descending order. By utilizing a 
 *     temporary stack we can make sure that all elements entered into the 
 *     final stack are ordered correctly. If an element that is attempting to be
 *     inserted is smaller than the top element of the final stack, all elements
 *     after said element whose data is greater than the insertion elements must
 *     be pushed into the temp stack. After which the original element is
 *     inserted and all elements from the temp stack are placed into the final
 *     stack
 * @param start  Original stack holding elements.
 * @param middle Used as a temporary storage location when sorting the final
 *                   stack.
 * @param end    Final stack, is sorted in descending order.
 */
void sort(Stack& start, Stack& middle, Stack& end) {
    // Begin stack relocation and sorting
    end.Push(start.Top()->data());
    start.Pop();
    while (!start.IsEmpty()) {
        if (start.Top()->data() > end.Top()->data()) {
            end.Push(start.Top()->data());
            start.Pop();
        } else {
            while (end.Top()->data() > start.Top()->data()) {
                middle.Push(end.Top()->data());
                end.Pop();
                if (end.IsEmpty()) {
                    break;
                }
            }
            end.Push(start.Top()->data());
            start.Pop();
            while (!middle.IsEmpty()) {
                end.Push(middle.Top()->data());
                middle.Pop();
            }
        }
    }
}
