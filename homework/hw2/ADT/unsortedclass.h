/** Copyright 2014 Barry C. Harris Jr. */
#include "string"

#include "../NBA/NBA.h"

#ifndef HOMEWORK_HW2_ADT_UNSORTEDCLASS_H_
#define HOMEWORK_HW2_ADT_UNSORTEDCLASS_H_

const int MAX_CLASS_LENGTH = 10;

/**
 * Class to handle "lists" of unsorted class objects (Namely NBA objects).
 */
class unsortedclass {
 public:
    unsortedclass();

    void add_player(NBA& new_player);

    int length() const;
    int Compare(NBA player1, NBA player2);
    int is_original(char first_initial, std::string last_name);
    NBA get_player_by_name(char first_initial, std::string name);
 private:
    int length_;
    NBA NBA_players[MAX_CLASS_LENGTH];
};
#endif  // HOMEWORK_HW2_ADT_UNSORTEDCLASS_H_
