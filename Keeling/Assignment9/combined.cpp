/** Time Specification **/

#include <cstdlib>
#include <iostream>
#include <string>

class Time {
    // *****Stream I/O *****
    // Write Times
    friend std::ostream & operator<< (std::ostream& outstream, const Time& time);
    // Read Times
    friend void operator>> (std::istream& instream, Time& time);

 private:
    int hours;
    int minutes;

 public:
    Time();
    Time(int hours, int minutes);

    //Time Length(void) const;
    void CopyTime(Time* t);

    // ***** relational operator *****
    // Time == Time
    bool operator== (Time& s) const;
    // Time < Time
    bool operator< (Time& s) const;
    // Time > Time
    bool operator> (Time& s) const;
    // Time <= Time
    bool operator<= (Time& s) const;
    // Time >= Time
    bool operator>= (Time& s) const;
    // Time != Time
    bool operator!= (Time& s) const;

    // ***** Time Arithmetic operators ****
    // ***** Time addition *****
    Time operator+ (Time& s) const;
    // ***** Time subtraction *****
    Time operator- (Time& s) const;
    // ***** Time mutiplication *****
    Time operator* (int) const;
    // ***** Time division *****
    Time operator/ (int) const;
};

/** Time Implementation **/

Time::Time() {
    this->hours = 0;
    this->minutes = 0;
}

Time::Time(int hours, int minutes) {;
    this->hours = hours;
    this->minutes = minutes;
}

void Time::CopyTime(Time * other_time) {
    hours = other_time->hours;
    minutes = other_time->minutes;
}

std::ostream & operator<< (std::ostream & outstream, const Time & time) {
    return outstream << time.hours << ":" << time.minutes;
}

void operator>>(std::istream & instream, Time & time) {
    std::string new_hours, new_minutes;
    instream >> new_hours >> new_minutes;
    time.hours = std::stoi(new_hours);
    time.minutes = std::stoi(new_minutes);
}

bool Time::operator== (Time & other_time) const {
    return (hours == other_time.hours && minutes == other_time.minutes);
}

bool Time::operator< (Time & other_time) const {
    if ((hours < other_time.hours) || (hours == other_time.hours &&
                minutes < other_time.minutes)) {
        return true;
    } else {
        return false;
    }
}

bool Time::operator> (Time & other_time) const {
    if ((hours > other_time.hours) || (hours == other_time.hours &&
                minutes > other_time.minutes)) {
        return true;
    } else {
        return false;
    }
}

bool Time::operator<= (Time & other_time) const {
    if ((hours < other_time.hours) ||
                (hours == other_time.hours && minutes < other_time.minutes) ||
                (hours == other_time.hours && minutes == other_time.minutes)) {
        return true;
    } else {
        return false;
    }
}

bool Time::operator>= (Time & other_time) const {
    if ((hours > other_time.hours) ||
                (hours == other_time.hours && minutes > other_time.minutes) ||
                (hours == other_time.hours && minutes == other_time.minutes)) {
        return true;
    } else {
        return false;
    }
}

bool Time::operator!= (Time & other_time) const {
    return ((hours != other_time.hours) ||
                (hours == other_time.hours && minutes != other_time.minutes));
}

Time Time::operator+ (Time & other_time) const {
    int total_minutes = minutes + other_time.minutes;
    return (total_minutes > 60) ?
            Time((hours + other_time.hours + (total_minutes / 60)),
                    (total_minutes - 60)) :
            Time((hours + other_time.hours), (minutes + other_time.minutes));
}

Time Time::operator- (Time & other_time) const {
    int total_minutes = minutes - other_time.minutes;
    return (total_minutes < 0) ?
            Time((hours - other_time.hours - 1),
                    (total_minutes + 60)) :
            Time((hours - other_time.hours), (minutes - other_time.minutes));
}

Time Time::operator* (int multiplier) const {
    int total_minutes = minutes * multiplier;

    return (total_minutes > 60) ?
            Time((hours * multiplier) + (total_minutes / 60),
                    (total_minutes - 60)) :
            Time((hours * multiplier), (minutes * multiplier));
}

Time Time::operator/ (int divisor) const {
    double total_hours = hours / divisor;
    double total_minutes = minutes / divisor;
    if ((hours / divisor) == 0) {
        total_hours *= 60;
        if ((minutes / divisor) == 0) {
            total_minutes *= 60;
        }
    }
    return Time((int)total_hours, (int)total_minutes);

}

/** Driver  **/

int main() {
    Time T1(12, 30);
    std::cout << "The first time is " << T1 << std::endl;
    Time T2(14, 30);
    Time T3(14, 30);
    Time T4(1, 45);
    Time T5;
    Time T6(2, 50);

    std::cout << "The second time is " << T2 << std::endl;

    if (T1 > T2) {
        std::cout << T1 << " is later than " << T2 << std::endl;
    }
    else {
        std::cout << T2 << " is later than " << T1 << std::endl;
    }

    std::cout << "The third time is " << T3 << std::endl;

    if (T2 == T3) {
        std::cout << T2 << " is equal to " << T3 << std::endl;
    }
    else {
        std::cout << T2 << " is not equal to " << T3 << std::endl;
    }

    if (T2 == T1) {
        std::cout << T2 << " is equal to " << T1 << std::endl;
    }
    else {
        std::cout << T2 << " is not equal to " << T1 << std::endl;
    }

    T5 = T4 + T3;
    std::cout << T5 << " = " << T4 << " + " << T3 << std::endl;
    T3 = T5 - T4;
    std::cout << T3 << " = " << T5 << " - " << T4 << std::endl;
    T4 = T6 * 2;
    std::cout << T4 << " = " << T6 << " * " << 2 << std::endl;
    return 0;
}


/**
The first time is 12:30
The second time is 14:30
14:30 is later than 12:30
The third time is 14:30
14:30 is equal to 14:30
14:30 is not equal to 12:30
16:15 = 1:45 + 14:30
14:30 = 16:15 - 1:45
5:40 = 2:50 * 2
**/
