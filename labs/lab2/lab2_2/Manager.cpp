#include "Manager.h"

Manager::Manager() :Employee() {
    salary_ = 2000;
}

Manager::Manager(std::string full_name, int id, int salary)
                :Employee(full_name, id) {
    salary_ = salary;
}

int Manager::salary() {
    return salary_;
}

void Manager::set_salary(int salary) {
    salary = salary_;
}
