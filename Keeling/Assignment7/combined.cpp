#include <iostream>
#include <string>
#include <fstream>

/* Specifications */
class personType {
 public:
    std::string firstName, lastName;
    std::string Address;
    std::string City, State;
    int Zip;
 private:
    std::string SSN;
};

enum interestType {sports, theater, movies,
    picnics, running, dining, reading, technology, TV, romance, travel,
    photography, facebook, twitter, hiking, backpacking,
    cooking, birdwatching, roadtrips, family, yoga};

class clientType: public personType {
 public:
    clientType(); // constructor
    int clientID;
    char clientGender;
    interestType clientInterests[10];
    // overloaded assignment operator
    /* clientType& operator= (const clientType& s)
     {
        ****** ENTER YOUR CODE HERE*******
     }*/

    // declare member functions
    void set_clientID(int);
    void set_clientGender(char);
    void set_FirstName(std::string);
    void set_LastName(std::string);
    void set_Address (std::string, std::string, std::string);
    void set_City (std::string);
    void set_State(std::string);
    void set_Zip(int);
    int get_clientID();
    char get_clientGender();
    interestType get_clientInterests();
    interestType convert_interest(std::string);
    void print_Clients();
};

class nodeType {
 public:
    nodeType();
    ~nodeType();

    clientType info;
    nodeType * next;
};

class unsortedType {
 public:
    unsortedType();
    ~unsortedType();

    void add_item(clientType & item);
    void print();
 private:
    nodeType * head_;
};

std::string convert_interest_to_string(interestType interest);

clientType::clientType() {
    clientID = 0;
    clientGender = ' ';
}

void clientType::set_FirstName(std::string name) {
    firstName = name;
}

void clientType::set_LastName(std::string name) {
    lastName = name;
}

void clientType::set_Address (std::string s1, std::string s2, std::string s3) {
    Address = s1 + " " + s2 + " " + s3;
}

void clientType::set_City (std::string s) {
    City = s;
}

void clientType::set_State(std::string s) {
    State = s;
}

void clientType::set_Zip(int s) {
    Zip = s;
}

void clientType::set_clientID(int idnumber) {
    clientID = idnumber;
}

void clientType::set_clientGender(char genderletter) {
    clientGender = genderletter;
}

int clientType::get_clientID() {
    return clientID;
}

char clientType::get_clientGender() {
    return clientGender;
}

interestType clientType::get_clientInterests() {
    return clientInterests[0];
}

void clientType::print_Clients() {
    std::cout << clientID << " "<< firstName << " " << lastName << std::endl
    << Address << std::endl << City << " " << State << " " << Zip << std::endl
    << clientGender << std::endl
    << convert_interest_to_string(clientInterests[0]) << " "
    << convert_interest_to_string(clientInterests[1]) << std::endl << std::endl;
}

/* Implementation */

std::string convert_interest_to_string(interestType interest) {
    switch (interest) {
        case sports:
                return "sports";
                break;
        case theater:
                return "theater";
                break;
        case movies:
                return "movies";
                break;
        case picnics:
                return "picnics";
                break;
        case running:
                return "running";
                break;
        case dining:
                return "dining";
                break;
        case reading:
                return "reading";
                break;
        case technology:
                return "technology";
                break;
        case TV:
                return "TV";
                break;
        case romance:
                return "romance";
                break;
        case travel:
                return "travel";
                break;
        case photography:
                return "photography";
                break;
        case facebook:
                return "facebook";
                break;
        case twitter:
                return "twitter";
                break;
        case hiking:
                return "hiking";
                break;
        case backpacking:
                return "backpacking";
                break;
        case cooking:
                return "cooking";
                break;
        case birdwatching:
                return "birdwatching";
                break;
        case roadtrips:
                return "roadtrips";
                break;
        case family:
                return "family";
                break;
        case yoga:
                return "yoga";
                break;
        default:
                return "unknown";
       }
}

nodeType::nodeType() {
    this->next = NULL;
    this->info.clientID = 0;
    this->info.clientGender = '\0';
    this->info.firstName = "";
    this->info.lastName = "";
    this->info.Address = "";
    this->info.City = "";
    this->info.State = "";
    this->info.Zip = 0;
}

nodeType::~nodeType() {
    delete next;
}

unsortedType::unsortedType() {
    this->head_ = NULL;
}

unsortedType::~unsortedType() {
    nodeType * temp = new nodeType();
    while (temp != NULL) {
        temp = head_->next;
        delete head_;
        head = temp;
    }
}

void unsortedType::add_item(clientType & item) {
    nodeType * temp = new nodeType();
    temp->info = item;
    if (head_ == NULL) {
        head_ = temp;
    } else {
        temp->next = head_;
        head_ = temp;
    }
}

void unsortedType::print() {
    if (head_ == NULL) {
        std::cout << "Empty list" << std::endl << std::endl;
    } else if (head_->next == NULL) {
        head_->info.print_Clients();
    } else {
        nodeType * traverse = new nodeType();
        traverse = head_;
        while (traverse != NULL) {
            traverse->info.print_Clients();
            traverse = traverse->next;
        }
        delete traverse;
    }
}

void heading();
void New_Client();
void Unmatch_Client();
void Print_Match();
void Print_Free();
int mainMenu();
interestType convert_interest(std::string s);

int main () {
    heading();
    unsortedType maleClientList;
    unsortedType femaleClientList;
    std::ifstream infile;
    infile.open("infile.txt");
    std::string tempFirstName, tempLastName, tempAddressNum, tempAddressRd,
    tempAddressRest, tempCity, tempState, tempInt1, tempInt2;
    int tempID, tempZip, seqInt, menuSelection, maleIndex = 0, femaleIndex = 0;
    char tempGender;
    for (int i = 0; i < 11; ++i) {
        infile >> tempFirstName >> tempLastName >> seqInt
        >> tempID >> tempAddressNum >> tempAddressRd
        >> tempAddressRest >> tempCity >> tempState
        >> tempZip >> tempGender >> tempInt1 >> tempInt2;

        clientType temp_client;
        temp_client.set_FirstName(tempFirstName);
        temp_client.set_LastName(tempLastName);
        temp_client.set_clientID(tempID);
        temp_client.set_Address(tempAddressNum, tempAddressRd, tempAddressRest);
        temp_client.set_City(tempCity);
        temp_client.set_State(tempState);
        temp_client.set_Zip(tempZip);
        temp_client.set_clientGender(tempGender);
        temp_client.clientInterests[0] = convert_interest(tempInt1);
        temp_client.clientInterests[1] = convert_interest(tempInt2);
        temp_client.print_Clients();

       if (tempGender == 'M') {
            maleClientList.add_item(temp_client);
        } else {
            femaleClientList.add_item(temp_client);
        }
    }

    // print male clients
    std::cout << std::endl;
    std::cout << "*************MALE CLIENTS*****************" << std::endl
            << std::endl;
    maleClientList.print();

    // print female clients
    std::cout << "*************FEMALE CLIENTS*****************" << std::endl
            << std::endl;
    femaleClientList.print();

    // Display menu
    menuSelection = mainMenu();
    switch(menuSelection) {
    case 1:
            New_Client();
            break;
            return 0;
    case 2:
            Unmatch_Client();
            break;
            return 0;
    case 3:
            Print_Match();
            break;
            return 0;
    case 4:
            Print_Free();
            break;
            return 0;
    default:
            return 0;
    }
}

void heading() {
    std::cout << "Barry Harris" << std::endl;
    std::cout << "@02705468" << std::endl;
    std::cout << "SYCS-136 Computer Science II" << std::endl;
    std::cout << "Assignment 7" << std::endl;
    std::cout << "February 11, 2015" << std::endl;
    std::cout << std::endl << std::endl;
    std::cout << "-------------------------------------------" << std::endl;
}

int mainMenu() {
    int myselection;
    std::cout << "-------------------------------------------" << std::endl;
    std::cout << "|***** HU DATING APPLICATION*****" << std::endl;
    std::cout << "|1.Add New Client" << std::endl << "|2.Unmatch Client"
            << std::endl << "|3.Print Match" << std::endl << "|4.Print"
            << std::endl << "|5.Quit" << std::endl;
    std::cout << "-------------------------------------------" << std::endl;
    std::cout << "Enter Selction: ";
    std::cin >> myselection;
    return myselection;
}

void New_Client() {
   std::cout << "New_Client" << std::endl;
}

void Unmatch_Client() {
    std::cout << "Unmatch_Client" << std::endl;
}

void Print_Match() {
   std::cout<<"Print_Match" << std::endl;
}

void Print_Free() {
   std::cout << "Print_Free" << std::endl;
}

interestType convert_interest(std::string s) {
    if (s == "sports") {
        return sports;
     } else if (s == "theater") {
        return theater;
     } else if (s == "movies") {
        return movies;
     } else if (s == "picnics") {
        return picnics;
     } else if (s == "running") {
        return running;
     } else if (s == "dining") {
        return dining;
     } else if (s == "reading") {
        return reading;
     } else if (s == "technology") {
        return technology;
     } else if (s == "TV") {
        return TV;
     } else if (s == "romance") {
        return romance;
     } else if (s == "travel") {
        return travel;
     } else if (s == "photography") {
        return photography;
     } else if (s == "facebook") {
        return facebook;
     } else if (s == "twitter") {
        return twitter;
     } else if (s == "hiking") {
        return hiking;
     } else if (s == "backpacking") {
        return backpacking;
     } else if (s == "cooking") {
        return cooking;
     } else if (s == "birdwatching") {
        return birdwatching;
     } else if (s == "roadtrips") {
        return roadtrips;
     } else if (s == "family") {
        return family;
     } else {
        return yoga;
    }
}
