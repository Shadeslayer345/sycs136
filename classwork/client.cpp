#include "DVD.h"

/**
 * Main user interface for creating and defninig a DVD object.
 */
main() {
   DVD dvd1;
   dvd1.set_genre("Action");
   dvd1.set_title("Batman");
   dvd1.set_year(2014);
   dvd1.print_data();
}
