#include <iostream>

#ifndef KEELING_TEST2_ADT_STUDENTTYPE_H
#define KEELING_TEST2_ADT_STUDENTTYPE_H
class studentType {
 public:
    studentType();

    void print();

    std::string first_name, last_name, city;
    int team_id;
};
#endif
