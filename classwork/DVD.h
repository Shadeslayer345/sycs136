#include <iostream>
#include <string>

#ifndef DVD_H
#define DVD_H

/**
 * Class to organize data for DVD format films
 */
class DVD {
   public:
      void set_genre(std::string genre);
      void set_title(std::string title);
      void set_year(int year);
      void print_data();
      std::string get_genre();
      std::string get_title();
      int get_year();
   private:
      std::string genre_, title_;
      int year_;
};
// endif DVD_H
