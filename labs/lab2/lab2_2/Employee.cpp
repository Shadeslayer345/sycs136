#include "Employee.h"

Employee::Employee() {
	full_name_ = "John Doe";
	id_ = 000000;
	payrate_ = 10;
}

Employee::Employee(std::string full_name, int id) {
	full_name_ = full_name;
	id_	= id;
}

Employee::Employee(std::string full_name, int id, int payrate) {
	full_name_ = full_name;
	id_ = id;
	payrate_ = payrate;
}

int Employee::id() {
	return id_;
}

int Employee::payrate() {
	return payrate_;
}

std::string Employee::full_name() {
	return full_name_;
}

int Employee::total_pay(int hours_worked) {
	return hours_worked * payrate_;
}

void Employee::set_id(int id) {
	id_ = id;
}

void Employee::set_payrate(int payrate) {
	payrate_ = payrate;
}

void Employee::set_full_name(std::string full_name) {
	full_name_ = full_name;
}
