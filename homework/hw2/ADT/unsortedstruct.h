/** Copyright 2014 Barry C. Harris Jr. */
#include "string"

#include "../NFL/NFL.h"

#ifndef HOMEWORK_HW2_ADT_UNSORTEDSTRUCT_H_
#define HOMEWORK_HW2_ADT_UNSORTEDSTRUCT_H_

const int MAX_STRUCT_LENGTH = 10;

/**
 * Class to handle "lists" of unsorted struct objects (namely the NFL objects).
 */
class unsortedstruct {
 public:
    unsortedstruct();

    void add_player(NFL& new_player);

    int length() const;
    int Compare(NFL player1, NFL player2);
    int is_original(std::string name);
    NFL get_player_by_name(std::string name);
 private:
    int length_;
    NFL NFL_players[MAX_STRUCT_LENGTH];
};
#endif  // HOMEWORK_HW2_ADT_UNSORTEDSTRUCT_H_
