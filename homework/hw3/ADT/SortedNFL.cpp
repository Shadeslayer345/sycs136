/** Copyright 2014 Barry C. Harris Jr. */
#include "SortedNFL.h"

/**
 * Default constructor, sets the player's league to NFL.
 */
SortedNFL::SortedNFL() {
    head = NULL;
    name_ = "ROSTER";
}

/**
 * Function to evaluate two NFL players using the players name as a comparator.
 * @param  player1 NFL object
 * @param  player2 NFL object
 * @return 0 for player1 == player2
 *         1 for player1 > player2
 *         -1 for player1 < player2
 */
int SortedNFL::compare(const NFL& player1, const NFL& player2) {
    if (player1.last_name().compare(player2.last_name()) == 0) {
        if (player1.first_name().compare(player2.first_name()) == 0) {
            return 0;
        } else if (player1.first_name().compare(player2.first_name()) > 0) {
            return 1;
        } else {
            return -1;
        }
    } else if (player1.last_name().compare(player2.last_name()) > 0) {
        return 1;
    } else {
        return -1;
    } 
}


/**
 * Displays the name of the lists as defined by the users or the default 
 *     constructor.
 */
std::string SortedNFL::name() const {
    return name_;
}

/**
 * Sets the name of the list
 * @param name String that will be the lists new name.
 */
void SortedNFL::set_name(std::string name) {
    name_ = name;
}

/**
 * Adds a NFL player object as the data for a new node to be placed in the
 *     linked list.
 * @param next_player NFL object that will serve as the data for the next
 *     element.
 */
void SortedNFL::add_player(NFL& next_player) {
    // Create a new node, settings its data and poiting to the next element.
    Node* new_player = new Node();
    new_player->set_data(next_player);
    new_player->set_next(NULL);

    Node *traverse = head;

    // Inserting new node into list.
    if (head == NULL || compare(new_player->data(), head->data()) < 0) {
            new_player->set_next(head);
            head = new_player;
    } else if (head->next() == NULL) {
        head->set_next(new_player);
    } else {
        while (traverse->next() != NULL ||
                compare(new_player->data(), traverse->next()->data()) >= 0) {
            traverse = traverse->next();
        }
        new_player->set_next(traverse);
        traverse = new_player;
    }
}

/**
 * Here we will traverse the length of the list and print the data from each
 *     element.
 */
void SortedNFL::print() {
    // Empty List, Single element list, or Multiple element list
    if (head == NULL) {
        std::cout << "Empty List" << std::endl;
    } if (head->next() == NULL) {
        std::cout << name_ << ":"<< std::endl;
        head->show_data();
        std::cout << std::endl;
    } else {
        std::cout << "Roster: " << name_ << ":"<< std::endl;
        Node *tmp = head;
        while (tmp != NULL) {
            tmp->show_data();
            std::cout << std::endl;
            tmp = tmp->next();
        }
    }
}
