/** Copyright 2014 Barry C. Harris Jr. **/

#include <iostream>
#include <new>

#include "Node.h"

#ifndef HOMEWORK_HW4_ADT_STACK_H
#define HOMEWORK_HW4_ADT_STACK_H

class FullStack {};
class EmptyStack {};

class Stack {
 public:
    Stack(); // Constructor

    bool IsEmpty() const;
    bool IsFull() const;
    void Pop();
    void Push(int item);
    Node* Top();
 private:
    Node* top;
};
#endif
