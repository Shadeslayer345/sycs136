#include "clientType.h"

#ifndef KEELING_ASSIGNMENT7_ADT_NODETYPE_H
#define KEELING_ASSIGNMENT7_ADT_NODETYPE_H
class nodeType {
 public:
    nodeType();
    ~nodeType();

    clientType info;
    nodeType * next;
};
#endif

#ifndef KEELING_ASSIGNMENT7_ADT_UNSORTEDTYPE_H
#define KEELING_ASSIGNMENT7_ADT_UNSORTEDTYPE_H
class unsortedType {
 public:
    unsortedType();
    ~unsortedType();

    void add_item(clientType & item);
    void print();
 private:
    nodeType * head_;
};
#endif
