#include "./time.h"

Time::Time() {
    this->hours = 0;
    this->minutes = 0;
}

Time::Time(int hours, int minutes) {;
    this->hours = hours;
    this->minutes = minutes;
}

void Time::CopyTime(Time * other_time) {
    hours = other_time->hours;
    minutes = other_time->minutes;
}

std::ostream & operator<< (std::ostream & outstream, const Time & time) {
    return outstream << time.hours << ":" << time.minutes;
}

void operator>>(std::istream & instream, Time & time) {
    std::string new_hours, new_minutes;
    instream >> new_hours >> new_minutes;
    time.hours = std::stoi(new_hours);
    time.minutes = std::stoi(new_minutes);
}

bool Time::operator== (Time & other_time) const {
    return (hours == other_time.hours && minutes == other_time.minutes);
}

bool Time::operator< (Time & other_time) const {
    if ((hours < other_time.hours) || (hours == other_time.hours &&
                minutes < other_time.minutes)) {
        return true;
    } else {
        return false;
    }
}

bool Time::operator> (Time & other_time) const {
    if ((hours > other_time.hours) || (hours == other_time.hours &&
                minutes > other_time.minutes)) {
        return true;
    } else {
        return false;
    }
}

bool Time::operator<= (Time & other_time) const {
    if ((hours < other_time.hours) ||
                (hours == other_time.hours && minutes < other_time.minutes) ||
                (hours == other_time.hours && minutes == other_time.minutes)) {
        return true;
    } else {
        return false;
    }
}

bool Time::operator>= (Time & other_time) const {
    if ((hours > other_time.hours) ||
                (hours == other_time.hours && minutes > other_time.minutes) ||
                (hours == other_time.hours && minutes == other_time.minutes)) {
        return true;
    } else {
        return false;
    }
}

bool Time::operator!= (Time & other_time) const {
    return ((hours != other_time.hours) ||
                (hours == other_time.hours && minutes != other_time.minutes));
}

Time Time::operator+ (Time & other_time) const {
    int total_minutes = minutes + other_time.minutes;
    return (total_minutes > 60) ?
            Time((hours + other_time.hours + (total_minutes / 60)),
                    (total_minutes - 60)) :
            Time((hours + other_time.hours), (minutes + other_time.minutes));
}

Time Time::operator- (Time & other_time) const {
    int total_minutes = minutes - other_time.minutes;
    return (total_minutes < 0) ?
            Time((hours - other_time.hours - 1),
                    (total_minutes + 60)) :
            Time((hours - other_time.hours), (minutes - other_time.minutes));
}

Time Time::operator* (int multiplier) const {
    int total_minutes = minutes * multiplier;

    return (total_minutes > 60) ?
            Time((hours * multiplier) + (total_minutes / 60),
                    (total_minutes - 60)) :
            Time((hours * multiplier), (minutes * multiplier));
}

Time Time::operator/ (int divisor) const {
    double total_hours = hours / divisor;
    double total_minutes = minutes / divisor;
    if ((hours / divisor) == 0) {
        total_hours *= 60;
        if ((minutes / divisor) == 0) {
            total_minutes *= 60;
        }
    }
    return Time((int)total_hours, (int)total_minutes);

}
