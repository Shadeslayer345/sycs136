/** Copyright 2014 Barry C. Harris Jr. */
#include "unsortedstruct.h"

/**
 * 
 */
unsortedstruct::unsortedstruct() {
    length_ = 0;
}

/**
 * Adds a player to the list
 * @param new_player A NFL class object.
 */
void unsortedstruct::add_player(NFL& new_player) {
    if (length_ < (MAX_STRUCT_LENGTH)) {
        NFL_players[length_] = new_player;
    } else {
        NFL_players[MAX_STRUCT_LENGTH-1] = new_player;
    }
    length_++;
}

/**
 * Returns the number of NFL objects in the list.
 * @return Length of the array.
 */
int unsortedstruct::length() const {
    return length_;
}

/**
 * Tests for equivalancy between two NBA objects.
 * @param  player1 NBA object.
 * @param  player2 NBA object.
 * @return The outcome, zero is they are equal and 1 if not.
 */
int unsortedstruct::Compare(NFL player1, NFL player2) {
    if ((player1.name.compare(player2.name)) &&
            (player1.school.compare(player2.school)) &&
            (player1.position.compare(player2.position)) &&
            (player1.team.compare(player2.team))) {
            
        return 0;
    } else {
        return 1;
    }
}

/**
 * Tests to see if an object being inserted is already present, this tests for
 *     the only constant value between the objects: name as no two players 
 *     should have the same name.
 * @param  name
 * @return Outcome, zero if not found and one if found.
 */
int unsortedstruct::is_original(std::string name) {
    for (int i = 0; i < length_; ++i) {
        NFL current_player = NFL_players[i];
        if (name.compare(current_player.name) != 0) {
            return 0;
        }
    }
    
    return 1;
}

/**
 * Retrieves a player from the array, assuming that each player has a unique
 *     name.
 * @param  first_initial
 * @param  last_name
 * @return An NFL object from the list that matches the query.
 */
NFL unsortedstruct::get_player_by_name(std::string name) {
    for (int i = 0; i < length_; ++i) {
        NFL current_player = NFL_players[i];
        if (name.compare(current_player.name)) {
            return current_player;
        }
    }

    // If player not found empty player will be returned
    std::cout << "Name not found! Retruning empty player instead!" << std::endl;
    NFL empty_player;
    return empty_player;
}
