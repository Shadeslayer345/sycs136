#include <iostream>
#include <fstream>
#include "personType.h"
#include <string>
using namespace std;
int main()
{
    personType members[6];
    string x, i1, i2;
    ifstream myfile;
    myfile.open("infile.txt");
    for( int i = 0; i < 6; i++)
    {
        myfile >> members[i].lastName  >> members[i].firstName >>
            members [i].personNum >> members[i].personID >>
            members [i].address.streetAddressNum >>
            members[i].address.streetAddress >> members [i].address.streetDec >>
            members[i].address.city >> members[i].address.stateInitials >>
            members[i].address.zipCode >> members [i].gender;
        myfile>> i1 >> i2; 
        members [i].setInterest1(i1);
        members [i].setInterest2(i2);
        members [i].print();
    }    
    myfile.close();
    cin.get();    
    return 0;
}

/**
 * Name: Jill Herold,
Person Number: 1
Person ID: 2234
Gender: F
Address: 123 Main St.Washington, DC 20019

Name: Stan Jackson,
Person Number: 2
Person ID: 3748
Gender: M
Address: 12 Douglas Ave.Baltimore, MD 30229

Name: Francis Jerry,
Person Number: 3
Person ID: 6666
Gender: M
Address: 2345 6th StreetWoodbridge, VA 44040

Name: Wilson Joan,
Person Number: 4
Person ID: 1234
Gender: F
Address: 12 Georgia Ave.Washington, DC 20019

Name: Stanley Smith,
Person Number: 5
Person ID: 3456
Gender: M
Address: 56 D StreetBaltimore, MD 30229

Name: Claude Claire,
Person Number: 6
Person ID: 2311
Gender: F
Address: 66 32nd StreetWoodbridge, VA 44040
 */
