#include <fstream>
#include <iostream>
#include "ADT/routeType.h"

void getFlightData(std::ifstream& flight_info, routeType all_flights[]);
void bookPassengers(routeType all_flights[], int flight_selection, int passengers_to_book);
void Print_Booking_Report(routeType all_flights[]);

int main() {
    routeType Flight_array[10];
    int flight_num = 1;
    int passengers = 1;
    std::ifstream myfile;
    myfile.open("infile.txt");
    getFlightData(myfile, Flight_array);
    myfile.close();
    while (flight_num != 0 && passengers != 0) {
        std::cout << "Enter the flight number for the fight you'd like to book ";
        std::cin >> flight_num;
        std::cin.get();
        std::cout << std::endl <<
            "Enter the number of passengers you would like to book for ";
        std::cin >> passengers;
        std::cin.get();
        if (flight_num == 0 && passengers == 0) {
            break;
        }
        std::cout << "Finding out flight information" << std::endl << std::endl;
        bookPassengers(Flight_array, flight_num, passengers);
    }
    std::cin.get();
    std::cout << "Printing Booking Report" << std::endl << std::endl;
    Print_Booking_Report(Flight_array);
    return 0;
}

void getFlightData(std::ifstream& flight_info, routeType all_flights[]) {
    int capacity = 0;
    int passenger_count = 0;
    for (int i = 0; i < 10; i++) {
        flight_info >> all_flights[i].from_city >> all_flights[i].to_city;
        flight_info >> capacity;
        flight_info >> passenger_count;
        all_flights[i].capacity = capacity;
        all_flights[i].current_passenger_count = passenger_count;
    }
}

void bookPassengers(routeType all_flights[], int selection, int passengers_to_book) {
    if (all_flights[selection].capacity == all_flights[selection].current_passenger_count) {
        std::cout << "Sorry that flight is full!" << std::endl << std::endl;
    } else {
        all_flights[selection].current_passenger_count += passengers_to_book;
        std::cout << "You've been booked for the flight!" << std::endl <<
            std::endl;
    }
}

void Print_Booking_Report(routeType all_flights[]) {
    std::cout << "Flight reports" << std::endl;
    for (int i = 0; i < 10; i++) {
        std::cout << "Flight " << i << ":" << std::endl
            << "From: " << all_flights[i].from_city << std::endl
            << "To: " << all_flights[i].to_city << std::endl
            << "Capacity: " << all_flights[i].capacity << std::endl
            << "Current Booked Passengers: " << all_flights[i].current_passenger_count
            << std::endl << std::endl << std::endl;
    }
}

/**
 * Enter the flight number for the fight you'd like to book 3

Enter the number of passengers you would like to book for 1
Finding out flight information

You've been booked for the flight!

Enter the flight number for the fight you'd like to book 0

Enter the number of passengers you would like to book for 0

Printing Booking Report

Flight reports
Flight 0:
From: Washington
To: Richmond
Capacity: 35
Current Booked Passengers: 34


Flight 1:
From: Huston
To: Chicago
Capacity: 50
Current Booked Passengers: 48


Flight 2:
From: Newark
To: Denver
Capacity: 40
Current Booked Passengers: 39


Flight 3:
From: Baltimore
To: Austin
Capacity: 25
Current Booked Passengers: 21


Flight 4:
From: Richmond
To: Denver
Capacity: 35
Current Booked Passengers: 33


Flight 5:
From: Washington
To: Austin
Capacity: 40
Current Booked Passengers: 39


Flight 6:
From: Denver
To: Richmond
Capacity: 70
Current Booked Passengers: 69


Flight 7:
From: Cleveland
To: Baltimore
Capacity: 20
Current Booked Passengers: 18


Flight 8:
From: Chicago
To: Washington
Capacity: 20
Current Booked Passengers: 19


Flight 9:
From: Huston
To: Cleveland
Capacity: 40
Current Booked Passengers: 40
 */
