#include "StackType.h"

StackType::StackType() {
    topPtr = NULL;
}

StackType::~StackType() {
    while (topPtr != NULL) {
        Pop();
    }
}

std::string StackType::Pop() {
    if (topPtr == NULL) {
        return "STACK EMPTY!";
    } else {
        std::string removedInfo = topPtr->info;
        NodeType* temp = new NodeType();
        temp = topPtr;
        topPtr = topPtr->next;
        delete temp;
        return removedInfo;
    }
}

void StackType::Push(std::string newWord ) {
    NodeType * newNode;
    newNode = new NodeType();
    newNode->info = newWord;
    newNode->next = topPtr;
    topPtr = newNode;
}
