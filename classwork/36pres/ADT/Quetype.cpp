#include "Quetype.h"

/**
 * Default Constructor with maxQue taking default value.
 */
QueType::QueType() {
    maxQue = 501; // Max value.
    front = maxQue - 1;
    rear = maxQue - 1;
    items = new char[maxQue];
}

/**
 * Parameterized Constructor that sets maxQue.
 */
QueType::QueType(int max) {
    maxQue = max + 1;
    front = maxQue - 1;
    rear = maxQue - 1;
    items = new char[maxQue];
}


bool QueType::IsEmpty() const {
    return (front == rear);
}

bool QueType::IsFull() const {
    return ((rear + 1) % maxQue) == front;
}

int QueType::Front() const {
    return items[front];
}

int QueType::Rear() const {
    return items[rear];
}

/**
 * Resets the Queue back to its initial state.
 */
void QueType::MakeEmpty() {
    front = maxQue - 1;
    rear = maxQue -1;
}

void QueType::Enque(char item) {
    if (IsFull()) {
        throw FullQue();
    } else {
        rear = rear + 1 % maxQue;
        items[rear] = item;
    }
}

void QueType::Deque() {
    if (IsEmpty()) {
        throw EmptyQue();
    } else {
        front = front + 1 % maxQue;
    }
}
