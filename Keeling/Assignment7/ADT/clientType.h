#include <iostream>
#include <string>

#ifndef KEELING_ASSIGNMENT7_ADT_PERSONTYPE_H
#define KEELING_ASSIGNMENT7_ADT_PERSONTYPE_H
class personType {
 public:
    std::string firstName, lastName;
    std::string Address;
    std::string City, State;
    int Zip;
 private:
    std::string SSN;
};
#endif

enum interestType {sports, theater, movies,
    picnics, running, dining, reading, technology, TV, romance, travel,
    photography, facebook, twitter, hiking, backpacking,
    cooking, birdwatching, roadtrips, family, yoga};

#ifndef KEELING_ASSIGNMENT7_ADT_CLIENTTYPE_H
#define KEELING_ASSIGNMENT7_ADT_CLIENTTYPE_H
class clientType: public personType {
 public:
    clientType(); // constructor
    int clientID;
    char clientGender;
    interestType clientInterests[10];
    // overloaded assignment operator
    /* clientType& operator= (const clientType& s)
     {
        ****** ENTER YOUR CODE HERE*******
     }*/

    // declare member functions
    void set_clientID(int);
    void set_clientGender(char);
    void set_FirstName(std::string);
    void set_LastName(std::string);
    void set_Address (std::string, std::string, std::string);
    void set_City (std::string);
    void set_State(std::string);
    void set_Zip(int);
    int get_clientID();
    char get_clientGender();
    interestType get_clientInterests();
    interestType convert_interest(std::string);
    void print_Clients();
};
#endif
