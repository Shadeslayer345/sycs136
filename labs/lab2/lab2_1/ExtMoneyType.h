#include "MoneyType.h"

#ifndef EXTMONEYTYPE_H
#define EXTMONEYTYPE_H
class ExtMoneyType:public MoneyType {
 public:
    ExtMoneyType(long, long, const std::string);
    std::string CurrencyIs() const;

 private:
    std::string currency_;
};
#endif
