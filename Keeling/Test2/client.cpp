#include <fstream>

#include "ADT/unsortedListType.h"
#include "ADT/studentType.h"

int main() {
    std::ifstream myfile;
    myfile.open("infile.txt");
    std::string student_first_name, student_last_name, student_city;
    int student_team_id;
    unsortedListType studentList;
    for (int i = 0; i < 35; ++i) {
        myfile >> student_first_name >> student_last_name >> student_team_id
                >> student_city;
        studentType temp_student;
        temp_student.first_name = student_first_name;
        temp_student.last_name = student_last_name;
        temp_student.team_id = student_team_id;
        temp_student.city = student_city;

        if ((student_team_id % 2) == 0) {
            studentList.insertFront(temp_student);
        } else {
            studentList.insertEnd(temp_student);
        }
    }

    studentType final_student;
    final_student.first_name = "Harry";
    final_student.last_name = "Keeling";
    final_student.team_id = 20;
    final_student.city = "Dallas";

    studentList.insertMid(final_student);

    studentList.printList();
    return 0;
}

/**
johan lingani  8 Woodbridge
Terrance Ellis  6 Columbia
Candace Ross  0 Rockville
victor foreman  4 Earlsville
Malcolm Williams  2 Winchester
Illium Williams  8 Juray
Ranjay Salmon  2 Trenton
Kareem Parris-Baptiste  8 Princeton
Blanche Mahop  6 Austin
crepin mahop  2 Bolder
Matthew Jacobs  8 Nashville
Allee Clark  4 Pittsburg
Charles Brown  0 Bowie
Dhuel Fisher  8 Richmord
Alyssa Bullock  6 Baltimore
Adegboyega Akinsiku  4 Topeka
Remington Holt  2 Detriot
Harry Keeling  20 Dallas
Kendall Keeling  3 Miami
Joanna Phillip  1 Tampa
jonnetta bratcher  3 Charleston
Kerisha Burke  7 Washington
Brionna Huskey  1 Sacramento
Brittany Jackson  5 Denver
Kourtnei Langley  9 Louisville
Ashlee McKinney  3 Houston
Jennifer Ojie  7 Arlington
Michael Phillips  3 Boston
Aja Walton  3 Boise
Kyle Ward  5 Germantown
Saboor Salaam  1 Reston
Cherith-Eden Clements  5 Hagerstown
Isa Edwards-El  3 Greensburg
Jaleesa Harrigan  1 Harrisonburg
Susan Angus  3 Burke
Rashad Rose  7 Fredricksburg
 */
