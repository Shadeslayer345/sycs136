#include "personType.h"

personType::personType() {
    firstName = "";
    lastName = "";
    personNum = 0;
    personID = 0;
    gender = '';
    this->address.streetAddressNum = "";
    this->address.streetAddress = "";
    this->address.streetDec = "";
    this->address.city = "";
    this->address.zipCode = "";
    this->address.stateInitials = "";
}

std::string personType::getInterest1() const {
    return interest1_;
}

std::string personType::getInterest2() const {
    return interest2_;
}

void personType::setInterest1(std::string new_interest1) {
    interest1_ = new_interest1;
}

void personType::setInterest2(std::string new_interest2) {
    interest2_ = new_interest2;
}

void personType::print() {
    std::cout << "Name: " << firstName << " " << lastName << std::endl <<
            "Person Number: " << personNum << std::endl <<
            "Person ID: " << personID << std::endl <<
            "Gender: " << gender << std::endl <<
            "Address: " << this->address.streetAddressNum << " " << 
            this->address.streetAddress << " " << this->address.streetDec <<
            this->address.city << " " << this->address.stateInitials << " " << 
            this->address.zipCode << std::endl << std::endl;
}
