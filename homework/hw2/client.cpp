/** Copyright 2014 Barry C. Harris Jr. */

#include <stdio.h>
#include <string>

#include "NBA/NBA.h"
#include "NFL/NFL.h"
#include "ADT/unsortedclass.h"
#include "ADT/unsortedstruct.h"

/**
 * The main client interface code. Will take the user's input and create either
 * an instance of the NBAplayer class or a NFLplayer struc and set the
 * respective objects data members to indicies in the the respective data types
 * list.
 */

/** Overloaded functions for both NFL and NBA players */
void create_player();
void set_player_name(NBA& player);
void set_player_current_team(NBA& player);
void set_player_position(NBA& player);
void player_confirmation(unsortedclass& roster, NBA& player);
void replay(unsortedclass& roster);

void create_player();
void set_player_name(NFL& player);
void set_player_current_team(NFL& player);
void set_player_position(NFL& player);
void set_player_school(NFL& player);
void player_confirmation(unsortedstruct& roster, NFL& player);
void replay(unsortedstruct& roster);

int main() {
    create_player();
    return 0;
}  // end main()

/**
 * Main code that handles the interface and function calls
 */
void create_player() {
    std::cout << "Hello!" << std::endl << "Welcome to the SportsFan, Inc. " <<
                "player tool!" << std::endl;
    std::cout << "Would you like to create a NBA(1) or NFL player(2)? ";

    int sport = 0;
    std::cin >> sport;

    std::cout << "You choose a " << ((sport == 1) ? "NBA" : "NFL") << " player!"
                << std::endl;
    std::cout << std::endl << "***WARNING***" << std::endl << "You can " <<
            "create multiple players but only of one type." << std::endl;

    if (sport == 1) {
        NBA player;
        unsortedclass roster;
        set_player_name(player);
        set_player_current_team(player);
        set_player_position(player);
        player_confirmation(roster, player);
    } else if (sport == 2) {
        NFL player;
        unsortedstruct roster;
        set_player_name(player);
        set_player_current_team(player);
        set_player_position(player);
        set_player_school(player);
        player_confirmation(roster, player);
    }
}  // end create player

/**
 * Recalls interface functions for all NBA objects after the first instance.
 * @param roster Unsorted list of NBA players.
 */
void replay(NBA& next_player, unsortedclass& roster) {
    std::cout << "Create your new player " << std::endl;

    set_player_name(next_player);
    set_player_current_team(next_player);
    set_player_position(next_player);
    player_confirmation(roster, next_player);
}

/**
 * Recalls the interface functions for all NFL objects after the first instance.
 * @param roster Unsorted list of NFL players
 */
void replay(NFL& next_player, unsortedstruct& roster) {
    std::cout << "Create your new player " << std::endl;

    set_player_name(next_player);
    set_player_current_team(next_player);
    set_player_position(next_player);
    set_player_school(next_player);
    player_confirmation(roster, next_player);
}

/**
 * Sets the name for a NBA object.
 * @param player Instance of the NBA class.
 */
void set_player_name(NBA& player) {
	char player_first_initial = '\0';
    std::string player_last_name = "";
    
    std::cout << "What is this NBA player's first initial? ";
    std::cin.get();
    player_first_initial = getchar();

    std::cout << "Now what is this player's last name? ";

    std::cin.get();
    getline(std::cin, player_last_name);

    player.set_first_initial(player_first_initial);
    player.set_last_name(player_last_name);
}  // end set player name

/**
 * Sets the name for a NFL object.
 * @param player Instance of the NFL class.
 */
void set_player_name(NFL& player) {
    std::string player_name = "";

    std::cout << "Now what is this player's name? ";

    std::cin.get();
    getline(std::cin, player_name);

    player.name = player_name;
}  // end set player name

/**
 * Sets the team for a NBA object.
 * @param player Instance of the NBA class.
 */
void set_player_current_team(NBA& player) {
    std::string current_team = "";

    std::cout << "What team in the NBA"<< " does " << player.get_first_initial()
            << ". " << player.get_last_name() << " play for? ";

    getline(std::cin, current_team);

    player.set_team(current_team);
}  // end set player current team

/**
 * Sets the team for a NFL object.
 * @param player Instance of the NFL class.
 */
void set_player_current_team(NFL& player) {
    std::string current_team = "";

    std::cout << "What team in the " << player.league << " does " << player.name
            << " play for? ";

    getline(std::cin, current_team);

    player.team = current_team;
}  // end set player current team

/**
 * Sets the position for a NBA object.
 * @param player Instance of the NBA class.
 */
void set_player_position(NBA& player) {
    char position = '\0';

    std::cout << "What position does " << player.get_first_initial() << ". " << 
            player.get_last_name() << " play on the " << player.get_team() << 
            "? " << std::endl <<
            "(P = Point Guard, S = Shooting Guard C = center) ";

    position = getchar();

    player.set_position(position);
}  // end set player position

/**
 * Sets the position for the NFL object.
 * @param player Instance of the NFL class.
 */
void set_player_position(NFL& player) {
    std::string position = "";

    std::cout << "What position does " << player.name << " play on the " <<
                player.team << "? ";

    getline(std::cin, position);

    player.position = position;
}  // end set player position

/**
 * Sets the school for the NFL object.
 * @param player Instance of the NFL class.
 */
void  set_player_school(NFL& player) {
    std::string school = "";

    std::cout << "And finally, what school did " << player.name <<
                " attend before entering the " << player.league << "? ";

    getline(std::cin, school);

    player.school = school;
}  // end set player alma mater

/**
 * Establish correctness of the created NBA object and inserts into to the
 *     unsorted list of NBA objects.
 * @param player Instance of the NBA class.
 */
void player_confirmation(unsortedclass& roster, NBA& player) {
    std::cout << "Okay does this look correct? " << std::endl;
    player.display_NBA();
    std::cout << "(y/n) ";

    std::cin.get();
    char finished = getchar();

    if (finished == 'y') {
        roster.add_player(player);
        std::cout << "Thank you for creating a player! You now have " <<
                roster.length() << std::endl <<
                " Would you like to create another?(Y/N) ";
        getchar();
        if (getchar() == 'y') {
            replay(player, roster);
        } else {
            std::cout << "Goodbye!" << std::endl;
        }
    } else {
        std::cout << "Sorry, please run the program again to create a new " <<
                    "player!!" << std::endl;
    }
}  // end confirmation

/**
 * Establish correctness of the created NFL object and insert into the 
 *     unsorted list of NFL objects.
 * @param player Instance of the NFL class.
 */
void player_confirmation(unsortedstruct& roster, NFL& player) {
    std::cout << "Okay does this look correct? " << std::endl;
    std::cout << "League: NFL" << std::endl <<
            "(1) Name: " << player.name << std::endl <<
            "(2) Current Team: " << player.team << std::endl <<
            "(3) Position: " << player.position << std::endl <<
            "(4) School: " << player.school << std::endl;
    std::cout << "(y/n) ";

    char finished = getchar();

    if (finished == 'y') {
        roster.add_player(player);
        std::cout << "Thank you for creating a player! You now have " <<
                roster.length() << std::endl <<
                " Would you like to create another?(Y/N) ";
        getchar();
        if (getchar() == 'y') {
            replay(player, roster);
        } else {
            std::cout << "Goodbye!" << std::endl;
        }
    } else {
        std::cout << "Sorry, please run the program again to create a new " <<
                    "player!!" << std::endl;
    }
}  // end confirmation


