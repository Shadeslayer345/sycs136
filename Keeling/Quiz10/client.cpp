#include <fstream>

#include "ADT/StackType.h"

int main() {
    StackType cityStack;
    std::ifstream infile;
    infile.open("infile.txt");
    std::string tempCity = "";
    infile >> tempCity;
    while (infile) {
        cityStack.Push(tempCity);
        infile >> tempCity;
    }
    for (int i = 0; i < 3; ++i) {
        std::cout << cityStack.Pop() << std::endl;
    }

    return 0;
}

/**
Dallas
Chicago
Austin
 */
