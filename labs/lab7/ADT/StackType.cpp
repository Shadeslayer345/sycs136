/** Copyright 2014 Barry C. Harris Jr. **/

#include "StackType.h"

StackType::StackType() {
    top = -1;
}

bool StackType::IsEmpty() const {
    return (top == -1);
}

bool StackType::IsFull() const {
    return (top == MAX_ITEMS - 1);
}

void StackType::Pop() {
    if (IsEmpty()) {
        throw EmptyStack();
    }
    top--;
}

void StackType::Push(char item) {
    if (IsFull()) {
        throw FullStack();
    }
    top++;
    items[top] = item;
}

char StackType::Top() const {
    if (IsEmpty()) {
        throw EmptyStack();
    }
    return items[top];
}
