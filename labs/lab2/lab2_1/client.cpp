#include <iostream>

#include "ExtMoneyType.h"


int main()
{
    MoneyType money(17, 32);

    std::cout << "$" << money.DollarsAre() << "." << money.CentsAre() <<
            std::endl;

    ExtMoneyType extmoney(11, 5, "$");
	
	// Determining if the cents are greater than 10 and require a prelimenary zero.
	std::string offset = 5 > 10 ? "" : "0";
	
    std::cout << extmoney.CurrencyIs() << "" << extmoney.DollarsAre() << "." <<
            offset << extmoney.CentsAre() << std::endl;
    return 0;
}
