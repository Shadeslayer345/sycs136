#include "Employee.h"

#ifndef MANAGER_H
#define MANAGER_H
class Manager:public Employee {
 public:
    Manager();
    Manager(std::string full_name, int id, int salary);
    int salary();
    void set_salary(int salary);

 private:
    int salary_;
};
#endif  //End of Manager class definition.
