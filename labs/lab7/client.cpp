#include <iostream>
#include <string>

#include "ADT/StackType.h"
#include "ADT/DynamicStack.h"

bool IsOpen(char symbol);
bool IsClosed(char symbol);
bool Matches (char symbol, char open_symbol);

int main() {
    bool well_formed = true;
    char symbol;
    char open_symbol;
    //StackType stack; // Array based stack
    DynamicStack stack; // Linked-List based stack
    try {
        std::cout << "Enter an expression and press return. " << std::endl;
        std::cin.get(symbol);
        while (symbol != '\n' && well_formed) {
            if (IsOpen(symbol)) {
                stack.Push(symbol);
            } else if (IsClosed(symbol)) {
                if (stack.IsEmpty()) {
                    well_formed = false;
                } else {
                    open_symbol = stack.Top();
                    stack.Pop();
                    well_formed = Matches(symbol, open_symbol);
                }
            }
            std::cin.get(symbol);
        }
    } catch (FullStack error) {
        std::cout << "Error! Push called when stack is full!!" << std::endl;
        return 1;
    } catch (EmptyStack error) {
        std::cout << "Error! Top/Pop called when stack is empty!!" << std::endl;
    } if (well_formed) {
        std::cout << "This expression is well formed" << std::endl;
    } else {
        std::cout << "This expression is not well formed" << std::endl;
    }

    return 0;
}

bool IsOpen(char symbol) {
    if (symbol == '(' || symbol == '{' || symbol == '[') {
        return true;
    } else {
        return false;
    }
}

bool IsClosed(char symbol) {
    if (symbol == ')' || symbol == '}' || symbol == ']') {
        return true;
    } else {
        return false;
    }
}

bool Matches (char symbol, char open_symbol) {
    if ((open_symbol == '(' && symbol == ')') || 
            (open_symbol == '{' && symbol == '}') || 
            (open_symbol == '[' && symbol == ']')) {
        return true;
    } else {
        return false;
    }
}
