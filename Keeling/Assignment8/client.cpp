#include <fstream>

#include "ADT/queType.h"

int main() {
    std::ifstream infile;
    infile.open("infile.txt");
    queType que1;
    for (int i = 0; i < 6; ++i) {
        std::string temp = "";
        infile >> temp;
        que1.enqueue(temp);
    }

    for (int i = 0; i < 3; ++i) {
        que1.dequeue();
    }

    return 0;
}
