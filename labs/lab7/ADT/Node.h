/** Copyright 2014 Barry C. Harris Jr. **/
#include <iostream>

#ifndef LABS_LAB7_ADT_Node
#define LABS_LAB7_ADT_Node
class Node {
 public:
    void show_data();
    char data() const;
    Node* next();

    void set_data(char data);
    void set_next(Node *next);
 private:
    char data_;
    Node* next_;
};  // end Node class declaration
#endif
