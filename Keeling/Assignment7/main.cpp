#include <fstream>

#include "ADT/unsortedType.h"

void heading();
void New_Client();
void Unmatch_Client();
void Print_Match();
void Print_Free();
int mainMenu();
interestType convert_interest(std::string s);

int main () {
    heading();
    unsortedType maleClientList;
    unsortedType femaleClientList;
    std::ifstream infile;
    infile.open("infile.txt");
    std::string tempFirstName, tempLastName, tempAddressNum, tempAddressRd,
    tempAddressRest, tempCity, tempState, tempInt1, tempInt2;
    int tempID, tempZip, seqInt, menuSelection, maleIndex = 0, femaleIndex = 0;
    char tempGender;
    for (int i = 0; i < 11; ++i) {
        infile >> tempFirstName >> tempLastName >> seqInt
        >> tempID >> tempAddressNum >> tempAddressRd
        >> tempAddressRest >> tempCity >> tempState
        >> tempZip >> tempGender >> tempInt1 >> tempInt2;

        clientType temp_client;
        temp_client.set_FirstName(tempFirstName);
        temp_client.set_LastName(tempLastName);
        temp_client.set_clientID(tempID);
        temp_client.set_Address(tempAddressNum, tempAddressRd, tempAddressRest);
        temp_client.set_City(tempCity);
        temp_client.set_State(tempState);
        temp_client.set_Zip(tempZip);
        temp_client.set_clientGender(tempGender);
        temp_client.clientInterests[0] = convert_interest(tempInt1);
        temp_client.clientInterests[1] = convert_interest(tempInt2);
        temp_client.print_Clients();

       if (tempGender == 'M') {
            maleClientList.add_item(temp_client);
        } else {
            femaleClientList.add_item(temp_client);
        }
    }

    // print male clients
    std::cout << std::endl;
    std::cout << "*************MALE CLIENTS*****************" << std::endl
            << std::endl;
    maleClientList.print();

    // print female clients
    std::cout << "*************FEMALE CLIENTS*****************" << std::endl
            << std::endl;
    femaleClientList.print();

    // Display menu
    menuSelection = mainMenu();
    switch(menuSelection) {
    case 1:
            New_Client();
            break;
            return 0;
    case 2:
            Unmatch_Client();
            break;
            return 0;
    case 3:
            Print_Match();
            break;
            return 0;
    case 4:
            Print_Free();
            break;
            return 0;
    case 5:
            return 0;
    default:
            return 0;
    }
}

void heading() {
    std::cout << "Barry Harris" << std::endl;
    std::cout << "@02705468" << std::endl;
    std::cout << "SYCS-136 Computer Science II" << std::endl;
    std::cout << "Assignment 7" << std::endl;
    std::cout << "February 11, 2015" << std::endl;
    std::cout << std::endl << std::endl;
    std::cout << "-------------------------------------------" << std::endl;
}

int mainMenu() {
    int myselection;
    std::cout << "-------------------------------------------" << std::endl;
    std::cout << "|***** HU DATING APPLICATION*****" << std::endl;
    std::cout << "|1.Add New Client" << std::endl << "|2.Unmatch Client"
            << std::endl << "|3.Print Match" << std::endl << "|4.Print"
            << std::endl << "|5.Quit" << std::endl;
    std::cout << "-------------------------------------------" << std::endl;
    std::cout << "Enter Selction: ";
    std::cin >> myselection;
    return myselection;
}

void New_Client() {
   std::cout << "New_Client" << std::endl;
}

void Unmatch_Client() {
    std::cout << "Unmatch_Client" << std::endl;
}

void Print_Match() {
   std::cout<<"Print_Match" << std::endl;
}

void Print_Free() {
   std::cout << "Print_Free" << std::endl;
}

interestType convert_interest(std::string s) {
    if (s == "sports") {
        return sports;
     } else if (s == "theater") {
        return theater;
     } else if (s == "movies") {
        return movies;
     } else if (s == "picnics") {
        return picnics;
     } else if (s == "running") {
        return running;
     } else if (s == "dining") {
        return dining;
     } else if (s == "reading") {
        return reading;
     } else if (s == "technology") {
        return technology;
     } else if (s == "TV") {
        return TV;
     } else if (s == "romance") {
        return romance;
     } else if (s == "travel") {
        return travel;
     } else if (s == "photography") {
        return photography;
     } else if (s == "facebook") {
        return facebook;
     } else if (s == "twitter") {
        return twitter;
     } else if (s == "hiking") {
        return hiking;
     } else if (s == "backpacking") {
        return backpacking;
     } else if (s == "cooking") {
        return cooking;
     } else if (s == "birdwatching") {
        return birdwatching;
     } else if (s == "roadtrips") {
        return roadtrips;
     } else if (s == "family") {
        return family;
     } else {
        return yoga;
    }
}

