#include "unsortedListType.h"

nodeType::nodeType() {
    this->info.first_name = "";
    this->info.last_name = "";
    this->info.team_id = 0;
    this->info.city = "";
}

nodeType::~nodeType() {
    delete next;
 }

unsortedListType::unsortedListType() {
    this->firstPtr = NULL;
    length = 0;
 }

unsortedListType::~unsortedListType() {
    nodeType * temp = new nodeType();
    while (temp->next != NULL) {
        temp = firstPtr->next;
        delete firstPtr;
        firstPtr = temp;
    }
 }

 void unsortedListType::insertMid(studentType & new_student) {
    nodeType * traverse = new nodeType();
    traverse = firstPtr;
    nodeType * new_node = new nodeType();
    new_node->info = new_student;
    for (int i = 0; i < (length / 2) - 1; ++i) {
        traverse = traverse->next;
    }
    new_node->next = traverse->next;
    traverse->next = new_node;
    length++;
 }

 void unsortedListType::insertEnd(studentType & new_student) {
    nodeType * new_node = new nodeType();
    new_node->info = new_student;
    if (firstPtr == NULL) {
        firstPtr = new_node;
    } else {
        nodeType * traverse = new nodeType();
        traverse = firstPtr;
        while (traverse->next != NULL) {
            traverse = traverse->next;
        }
        traverse->next = new_node;
    }
    length++;
 }

 void unsortedListType::insertFront(studentType & new_student) {
    nodeType * new_node = new nodeType();
    new_node->info = new_student;
    if (firstPtr == NULL) {
        firstPtr = new_node;
    } else {
        new_node->next = firstPtr;
        firstPtr = new_node;
    }
    length++;
 }

void unsortedListType::printList() {
    if (firstPtr == NULL) {
        std::cout << "Empty list" << std::endl << std::endl;
    } else if (firstPtr->next == NULL) {
        firstPtr->info.print();
    } else {
        nodeType * traverse = new nodeType();
        traverse = firstPtr;
        while (traverse != NULL) {
            traverse->info.print();
            traverse = traverse->next;
        }
        delete traverse;
    }
}
