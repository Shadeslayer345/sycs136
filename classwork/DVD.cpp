#include "DVD.h"

/**
 * Changes the genre data memeber to a new value.
 * @param genre The new value for the genre data memember.
 */
void DVD::set_genre(std::string genre){
   genre_ = genre;}

/**
 * Changes the title data member to a new value.
 * @param title The new value for the title data member.
 */
void DVD::set_title(std::string title){
   title_ = title;}

/**
 * Changes the year data member to a new value.
 * @param year The new value for the year data member.
 */
void DVD::set_year(int year){
   year_ = year;}

/**
 * Retrieves the genre data member.
 * @return Value for genre data member.
 */
std::string DVD::get_genre(){
   return genre_;}

/**
 * Retrieves the tile data member. 
 * @return Value for the title data member.
 */
std::string DVD::get_title(){
   return title_;}

/**
 * Retrieves the year data member.
 * @return Value to the year data member.
 */
int DVD::get_year(){
   return year_;}

/**
 * Outputs all data members of the DVD class.
 */
void DVD::print_data(){
   std::cout << "genre: " << genre_ << std::endl
        << "title: " << title_ << std::endl
        << "year: " << year_ << std::endl;}
