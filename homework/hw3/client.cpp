/** Copyright 2014 Barry C. Harris Jr. */
#include <stdio.h>

#include "NFL.h"
#include "ADT/SortedNFL.h"

void create_players();
void set_player_name(NFL& player);
void set_player_team(NFL& player);
void set_player_position(NFL& player);
void set_player_school(NFL& player);
void player_confirmation(SortedNFL& roster, NFL& player);
void roster_confirmation(SortedNFL& roster);


int main() {
    create_players();
    return 0;
}

void create_players() {
    std::cout << "Hello!" << std::endl << "Welcome to the SportsFan, Inc. " <<
                "player tool!" << std::endl;
    std::cout << "How many NFL players would you like to create ? (1-10)? ";

    int count = 0;
    std::cin >> count;

    std::cout << "You choose " << count << " players!" << std::endl;

    SortedNFL roster;
    std::cout << "Would you like to name this roster (Y/N)? ";
    getchar();
    char choice = getchar();

    if (choice == 'y' || choice == 'Y') {
        std::string name = "";
        std::cout << "Enter roster name here: ";
        std::cin.get();
        getline(std::cin, name);
        roster.set_name(name);
    }

    for (int i = count; i > 0; i--) {   
        std::cout << std::endl << "Creating player " << count << std::endl;
        NFL player;
        set_player_name(player);
        set_player_team(player);
        set_player_position(player);
        set_player_school(player);
        player_confirmation(roster, player);
    }

    roster_confirmation(roster);
}

/**
 * Sets the name for a NFL object.
 * @param player Instance of the NFL class.
 */
void set_player_name(NFL& player) {
    std::string player_first_name = "";
    std::string player_last_name = "";

    std::cout << "Now what is this player's first name? ";
    getline(std::cin, player_first_name);

    std::cout << "Last name ? ";
    getline(std::cin, player_last_name);

    player.set_first_name(player_first_name);
    player.set_last_name(player_last_name);
}  // end set player name

/**
 * Sets the team for a NFL object.
 * @param player Instance of the NFL class.
 */
void set_player_team(NFL& player) {
    std::string current_team = "";

    std::cout << "What team in the " << player.league() << " does " <<
            player.first_name() << " " << player.last_name() << " play for? ";

    getline(std::cin, current_team);

    player.set_team(current_team);
}  // end set player current team

/**
 * Sets the position for the NFL object.
 * @param player Instance of the NFL class.
 */
void set_player_position(NFL& player) {
    std::string position = "";

    std::cout << "What position does " << player.first_name() << " " << 
                player.last_name() << " play on the " << player.team() << "? ";

    getline(std::cin, position);

    player.set_position(position);
}  // end set player position

/**
 * Sets the school for the NFL object.
 * @param player Instance of the NFL class.
 */
void  set_player_school(NFL& player) {
    std::string school = "";

    std::cout << "And finally, what school did " << player.first_name() <<
                " " << player.last_name() << " attend before entering the " <<
                player.league() << "? ";

    getline(std::cin, school);

    player.set_school(school);
}  // end set player alma mater

/**
 * Establish correctness of the created NFL object and insert into the 
 *     sorted list of NFL objects.
 * @param player Instance of the NFL class.
 */
void player_confirmation(SortedNFL& roster, NFL& player) {
    std::cout << std::endl << "This is the player you have created " << 
            "(No changes can be made at this point)" << std::endl;
            std::cout << "League: NFL" << std::endl <<
            "(1) Name: " << player.first_name() << " " << player.last_name() <<
            std::endl << "(2) Current Team: " << player.team() << std::endl <<
            "(3) Position: " << player.position() << std::endl <<
            "(4) School: " << player.school() << std::endl;
    roster.add_player(player);
}  // end player confirmation

void roster_confirmation(SortedNFL& roster) {
    std::cout << std::endl << "You have filled your roster!" << std::endl <<
            "Here is the roster you have created: "  << std::endl;

    roster.print();
}
