#include <iostream>

int fib(int n);

int refib(int n);

int main() {
    int i = 0;
    std::cout << "Enter the Nth term of the Fibonacci sequence that you'd " <<
            "like to solve for ";
    std::cin >> i;

    std::cout << "The Fibonacci sequence for "<< i <<" is " << fib(i) <<
            std::endl;

    std::cout << "The Fibonacci sequence for "<< i <<" is " << refib(i) << 
            std::endl;

    return 0;
}

int fib(int n) {
    if (n == 0 || n == 1) {
        return n;
    } else {
        int temp = 0;
        int current = 1;
        int previous = 1;
        for (int i = 2; i < n; ++i) {
            int temp = current;
            current = current + previous;
            previous = temp;
        } return current;
    }
}

int refib(int n) {
    if (n == 0 || n == 1) {
        return n;
    } else {
        return (refib(n-2) + refib(n-1));
    }
}
