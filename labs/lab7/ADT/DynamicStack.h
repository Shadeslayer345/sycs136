/** Copyright 2014 Barry C. Harris Jr. **/

#include <iostream>
#include <new>

#include "Node.h"
#include "StackType.h"

#ifndef LABS_LAB7_ADT_DYNAMICSTACK_H
#define LABS_LAB7_ADT_DYNAMICSTACK_H

class DynamicStack {
 public:
    DynamicStack(); // Constructor

    bool IsEmpty() const;
    bool IsFull() const;
    void Pop();
    void Push(char item);
    char Top() const;
 private:
    Node* top;
};
#endif
