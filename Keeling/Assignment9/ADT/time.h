#include <cstdlib>
#include <iostream>
#include <string>

class Time {
    // *****Stream I/O *****
    // Write Times
    friend std::ostream & operator<< (std::ostream& outstream, const Time& time);
    // Read Times
    friend void operator>> (std::istream& instream, Time& time);

 private:
    int hours;
    int minutes;

 public:
    Time();
    Time(int hours, int minutes);

    //Time Length(void) const;
    void CopyTime(Time* t);

    // ***** relational operator *****
    // Time == Time
    bool operator== (Time& s) const;
    // Time < Time
    bool operator< (Time& s) const;
    // Time > Time
    bool operator> (Time& s) const;
    // Time <= Time
    bool operator<= (Time& s) const;
    // Time >= Time
    bool operator>= (Time& s) const;
    // Time != Time
    bool operator!= (Time& s) const;

    // ***** Time Arithmetic operators ****
    // ***** Time addition *****
    Time operator+ (Time& s) const;
    // ***** Time subtraction *****
    Time operator- (Time& s) const;
    // ***** Time mutiplication *****
    Time operator* (int) const;
    // ***** Time division *****
    Time operator/ (int) const;
};
