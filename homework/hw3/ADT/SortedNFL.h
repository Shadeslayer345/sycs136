/** Copyright 2014 Barry C. Harris Jr. */
#include <iostream>
#include <string>

#include "../NFL.h"
#include "Node.h"

#ifndef HOMEWORK_HW3_ADT_SORTEDNFL_H
#define HOMEWORK_HW3_ADT_SORTEDNFL_H
/**
 * Class to handle information of NFL players in a linked list.
 */
class SortedNFL {
 public:
    SortedNFL();

    int compare(const NFL& player1, const NFL& player2);
    std::string name() const;

    void set_name(std::string name);
    void add_player(NFL& new_player);
    void print();
 private:
    Node* head;
    std::string name_;
    
};
#endif
