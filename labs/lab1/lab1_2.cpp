#include <iostream>
#include <string>

struct Cars {
    int make, year, price; 
};

void OutputResults(Cars output_car);

main() {
    Cars Car1;
    Car1.make = 1;
    Car1.year = 2010;
    Car1.price = 2100;

    OutputResults(Car1);
    OutputResults(Car1);


    Cars Car2;
    Car2 = Car1;

    OutputResults(Car2);
    OutputResults(Car2);

}

void OutputResults(Cars output_car) {
    std::cout << "Car struct" << std::endl << 
            "Make: " << output_car.make << 
            " (1:Honda 2:Ford 3:Nissan 4:Dodge)"<< std::endl << 
            "Year: " << output_car.year << std::endl <<
            "Price: " << output_car.price << std::endl;
}
