/** Copyright 2014 Barry C. Harris Jr. */
#include <iostream>
#include <string>

/**
 * Class to handle information of NFL players.
 */
#ifndef HOMEWORK_HW3_NFL_H
#define HOMEWORK_HW3_NFL_H
class NFL {
 public:
    NFL();

    std::string first_name() const;
    std::string last_name() const;
    std::string league() const;
    std::string position() const;
    std::string school() const;
    std::string team() const;

    void set_first_name(std::string first_name);
    void set_last_name(std::string last_name);
    void set_position(std::string position);
    void set_school(std::string school);
    void set_team(std::string team);

    void print();

 private:
    std::string team_, first_name_, last_name_, league_, position_,
            school_;
};
#endif
