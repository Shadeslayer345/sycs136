#include "contactType.h"

contactType::contactType() {
    first_name = "";
    last_name = "";
    phone_number_ptr = NULL;
}

contactType::contactType(std::string new_first_name, std::string new_last_name) {
    first_name = new_first_name;
    last_name = new_last_name;
    phone_number_ptr = NULL;
}

contactType::~contactType() {
    first_name = "";
    last_name = "";
    delete phone_number_ptr;
}
