#include <iostream>
#include <string>

#ifndef KEELING_QUIZ8_ADT_NODETYPE_H
#define KEELING_QUIZ8_ADT_NODETYPE_H
class NodeType {
 public:
    std::string info;
    NodeType * next;
};
#endif

#ifndef KEELING_QUIZ8_ADT_STACKTYPE_H
#define KEELING_QUIZ8_ADT_STACKTYPE_H
class StackType {
 public:
  StackType(); // Constructor
  ~StackType(); // Destructor
  void Push(std::string newWord); //Pushes string parameter onto top of stack
  std::string Pop(); //deletes the top node and returns string in node
  NodeType * topPtr;
};
#endif
