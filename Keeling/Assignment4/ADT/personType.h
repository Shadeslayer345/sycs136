#include <iostream>
#include <string>

#ifndef ADDRESSTYPE
#define ADDRESSTYPE
class addressType {
 public:
    std::string streetAddressNum, streetAddress, streetDec;
    std::string  city, stateInitials, zipCode;
};
#endif

#ifndef PERSONTYPE
#define PERSONTYPE
class personType {
 public:
    personType();

    std::string getInterest1() const;
    std::string getInterest2() const;
    void setInterest1(std::string new_interest1);
    void setInterest2(std::string new_interest2);
    void print();

    std::string lastName, firstName;
    int personNum, personID;
    char gender;
    addressType address;
 private:
    std::string interest1_, interest2_;
};
#endif
