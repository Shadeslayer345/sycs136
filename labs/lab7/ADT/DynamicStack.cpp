/** Copyright 2014 Barry C. Harris Jr. **/

#include "DynamicStack.h"

DynamicStack::DynamicStack() { // Default Constructor
    top = NULL;
}

bool DynamicStack::IsEmpty() const {
    return (top == NULL);
}

bool DynamicStack::IsFull() const {
    Node* newNode;
    try {
        newNode = new Node();
        delete newNode;
        return false;
    } catch (std::bad_alloc exception) {
        return true;
    }
}

void DynamicStack::Pop() {
    if (IsEmpty()) {
        throw EmptyStack();
    } else {
        Node* temp;
        temp = top;
        top = top->next();
        delete temp;
    }
}

void DynamicStack::Push(char newItem) {
    if (IsFull()) {
        throw FullStack();
    } else {
        Node* newNode;
        newNode = new Node();
        newNode->set_data(newItem);
        newNode->set_next(top);
        top = newNode;
    }
}

char DynamicStack::Top() const {
    if (IsEmpty()) {
        throw EmptyStack();
    } else {
        return top->data();
    }
}
