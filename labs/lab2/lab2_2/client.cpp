#include <iostream>

#include "Employee.h"
#include "Manager.h"

int main() {
	
	Employee worker;
    worker.set_full_name("John Lewis");
    worker.set_id(023111);
    worker.set_payrate(25);

    std::cout << "Name: " << worker.full_name() << std::endl <<
            "ID: " << worker.id() << std::endl <<
            "Payrate: $" << worker.payrate() << "/hr" << std::endl <<
            "Total pay: $" << worker.total_pay(40) << std::endl;

    Manager manager;
    manager.set_full_name("Bryan Lewis");
    manager.set_id (034222);
    manager.set_salary(50000);
	
    std::cout << std::endl << "Name: " << manager.full_name() << std::endl <<
            "ID: " << manager.id() << std::endl <<
            "Salary: $" << manager.salary() << "/yr" << std::endl;
	
    return 0;
}
