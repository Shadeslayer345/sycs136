/** Copyright 2014 Barry C. Harris Jr. */

#include <iostream>
#include <string>

#ifndef NFL_H
#define NFL_H

/**
 * Struct to hold relevant data for an NFLplayer
 */
struct NFL {
 public:
      	NFL();
      	NFL(std::string new_school, std::string new_name, std::string new_position, std::string new_team);

        std::string school, league, name, position, team;
};
#endif
