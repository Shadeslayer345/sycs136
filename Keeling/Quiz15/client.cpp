#include <cstdio>
#include <string>

long factorial(long num) {
    if (num <= 1) {
        return 1;
    } else {
        return num * factorial(num - 1);
    }
}

int main() {
    long a;
    do {
        printf("Enter an integer: ");
        scanf("%ld", &a);
        printf("%ld! = %ld\n", a, factorial(a));
    } while (a != 0);
}
