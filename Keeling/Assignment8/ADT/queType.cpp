#include "queType.h"

nodeType::nodeType() {
    this->info = "";
    this->next = NULL;
}

nodeType::~nodeType() {
    delete next;
}

queType::queType() {
    this->front_ = NULL;
    this->rear_ = NULL;
}

queType::~queType() {
    while (front_ && rear_ != NULL) {
        if (front_->next == NULL) {
            delete front_;
            front_ = NULL;
            rear_ = NULL;
        } else {
            front_ = front_->next;
        }
    }
}

void queType::enqueue(std::string item) {
    nodeType * temp = new nodeType();
    temp->info = item;
    if (rear_ == NULL) {
        rear_ = temp;
        front_ = rear_;
    } else {
        rear_->next = temp;
        rear_ = rear_->next;
    }
}

void queType::dequeue() {
    if (front_ == NULL) {
        std::cout << "Can't dequeue from empty queue" << std::endl;
    } else {
        std::cout << front_->info << std::endl;
        if (front_->next == NULL) {
            delete front_;
            delete rear_;
            front_ = NULL;
            rear_ = NULL;
        } else {
            front_ = front_->next;
        }
    }
}
