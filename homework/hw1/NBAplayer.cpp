/** Copyright 2014 Barry C. Harris Jr. */

#include "NBAplayer.h"

/**
 * Default constructor for the NBAplayer class which explicitly sets the league
 * data member.
 * @constructor
 */
Baller::Baller() {
    league_ = "NBA";
}

/**
 * Constructor for the NBAplayer class which allows an instance to be created
 * with all data members assigned.
 * @constructor
 */
Baller::Baller(std::string school, std::string name, char position,
                std::string team) {
    school_ = school;
    league_ = "NBA";
    name_ = name;
    position_ = position;
    team_ = team;
}

/**
 * Returns the value of the school data member.
 * @return The alma mater value.
 */
std::string Baller::school() const {
    return school_;
}

/**
 * Returns Th value of the league data member.
 * @return The alma mater value.
 */
std::string Baller::league() const {
    return league_;
}

/**
 * Returns The value of the name data member.
 * @return The name value
 */
std::string Baller::name() const {
    return name_;
}

/**
 * Retuns The value of the position data member.
 * @return The position value
 */
char Baller::position() const {
    return position_;
}

/**
 * Returns The value of the team data member.
 * @return The team value
 */
std::string Baller::team() const {
    return team_;
}

/**
 * Changes the value of the school data member.
 * @param school The new alma mater value.
 */
void  Baller::set_school(std::string school) {
    school_ = school;
}

/**
 * Changes the value of the name data member.
 * @param  name The new name value.
 */
void  Baller::set_name(std::string name) {
    name_ = name;
}

/**
 * Changes the value of the position data member.
 * @param  position The new position value.
 */
void  Baller::set_position(char position) {
    position_ = position;
}

/**
 * Changes the value of the team data member.
 * @param  team The new team value
 */
void  Baller::set_team(std::string team) {
    team_ = team;
}

/**
 * Prints all data members of the Baller.
 */
void Baller::display_baller() {
    std::cout << "League: NBA" << std::endl <<
            "(1) Name: " << name_ << std::endl <<
            "(2) Current Team: " << team_ << std::endl <<
            "(3) Position: " << position_ << std::endl <<
            "(4) School: " << school_ << std::endl;
}
