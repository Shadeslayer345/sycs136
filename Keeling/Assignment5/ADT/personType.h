#include <string>

#ifndef PERSONTYPE
#define PERSONTYPE
class personType
{
public:
    personType();
    std::string firstName, lastName;
    std::string personNum;
    char gender;
    std::string Address, AddressNum, AddressDec;
    std::string City, State, Zip;
};
#endif
