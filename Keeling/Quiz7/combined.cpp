// RouteType - Specification

#include <cstddef>
#include <fstream>
#include <iostream>
#include <string>

#ifndef KEELING_TEST1_ROUTETYPE_H
#define KEELING_TEST1_ROUTETYPE_H
class routeType {
 public:
    routeType();
    std::string from_city, to_city;
    int capacity, current_passenger_count;
};
#endif

// RouteType - Implementation

routeType::routeType() {
    from_city = "";
    to_city = "";
    capacity = 0;
    current_passenger_count = 0;
}


// Node - Specification
#ifndef KEELING_QUIZ7_NODETYPE_H
#define KEELING_QUIZ7_NODETYPE_H

typedef routeType itemType;

struct nodeType {
    itemType info;
    nodeType* next;
};
#endif


// Driver 

void getFlightData(std::ifstream& flight_info, nodeType* route_node);
void bookPassengers(nodeType* route_node, int flight_selection, int passengers_to_book);
void Print_Booking_Report(nodeType* route_node);

int main() {
    nodeType* first = new nodeType; // first node in linked-list
    int flight_num = 1;
    int passengers = 1;
    std::ifstream myfile;
    myfile.open("infile.txt");
    getFlightData(myfile, first);
    myfile.close();
    std::cout << "  WELCOME TO FLIGHT BOOKER 3000!" << std::endl;
    std::cout << "----------------------------------" << std::endl <<
            std::endl;
    while (flight_num != 0 && passengers != 0) {
        std::cout << "Enter the flight number for the fight you'd like to book ";
        std::cin >> flight_num;
        std::cin.get();
        std::cout << std::endl <<
            "Enter the number of passengers you would like to book for ";
        std::cin >> passengers;
        std::cin.get();
        if (flight_num == 0 && passengers == 0) {
            break;
        }
        std::cout << std::endl << "Finding out flight information"
                << std::endl << std::endl;
        bookPassengers(first, flight_num, passengers);
        std::cout << "--------------------------------------------" << std::endl;
    }
    std::cin.get();
    std::cout << std::endl << " Booking Report" << std::endl;
    std::cout << "-------------------" << std::endl;
    Print_Booking_Report(first);
    return 0;
}

void getFlightData(std::ifstream& flight_info, nodeType* route_node) {
    int capacity = 0;
    int passenger_count = 0;
    nodeType *current_route_node = route_node;
    flight_info >> (current_route_node)->info.from_city >>
            current_route_node->info.to_city;
    flight_info >> capacity;
    flight_info >> passenger_count;
    current_route_node->info.capacity = capacity;
    current_route_node->info.current_passenger_count = passenger_count;
    while (flight_info) {
        current_route_node->next = new nodeType;
        current_route_node = current_route_node->next;
        current_route_node->next = NULL;
        flight_info >> current_route_node->info.from_city >>
                current_route_node->info.to_city;
        flight_info >> capacity;
        flight_info >> passenger_count;
        current_route_node->info.capacity = capacity;
        current_route_node->info.current_passenger_count = passenger_count;
    }
}

void bookPassengers(nodeType* route_node, int selection, int passengers_to_book) {
    nodeType *current_route_node = route_node;
    for (int i = 0; i < selection; ++i) {
        current_route_node = current_route_node->next;
    }
    if (current_route_node->info.capacity ==
                current_route_node->info.current_passenger_count) {
        std::cout << "Sorry that flight is full!" << std::endl << std::endl;
    } else {
        current_route_node->info.current_passenger_count += passengers_to_book;
        std::cout << "You've been booked for the flight!" << std::endl <<
            std::endl;
    }
}

void Print_Booking_Report(nodeType* route_node) {
    nodeType* current_route_node = route_node;
    std::cout << "Flight reports" << std::endl;
    int i = 0;
    while (current_route_node->next != NULL) {
        std::cout << "Flight " << i << ":" << std::endl
            << "From: " << current_route_node->info.from_city << std::endl
            << "To: " << current_route_node->info.to_city << std::endl
            << "Capacity: " << current_route_node->info.capacity << std::endl
            << "Current Booked Passengers: " <<
            current_route_node->info.current_passenger_count
            << std::endl << std::endl << std::endl;
            current_route_node = current_route_node->next;
            i++;
    }
}

/**
 * Enter the flight number for the fight you'd like to book 3

Enter the number of passengers you would like to book for 1
Finding out flight information

You've been booked for the flight!

Enter the flight number for the fight you'd like to book 0

Enter the number of passengers you would like to book for 0

Printing Booking Report

Flight reports
Flight 0:
From: Washington
To: Richmond
Capacity: 35
Current Booked Passengers: 34


Flight 1:
From: Huston
To: Chicago
Capacity: 50
Current Booked Passengers: 48


Flight 2:
From: Newark
To: Denver
Capacity: 40
Current Booked Passengers: 39


Flight 3:
From: Baltimore
To: Austin
Capacity: 25
Current Booked Passengers: 21


Flight 4:
From: Richmond
To: Denver
Capacity: 35
Current Booked Passengers: 33


Flight 5:
From: Washington
To: Austin
Capacity: 40
Current Booked Passengers: 39


Flight 6:
From: Denver
To: Richmond
Capacity: 70
Current Booked Passengers: 69


Flight 7:
From: Cleveland
To: Baltimore
Capacity: 20
Current Booked Passengers: 18


Flight 8:
From: Chicago
To: Washington
Capacity: 20
Current Booked Passengers: 19


Flight 9:
From: Huston
To: Cleveland
Capacity: 40
Current Booked Passengers: 40
 */
