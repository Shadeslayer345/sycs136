/** Copyright 2014 Barry C. Harris Jr. */

#include <iostream>
#include <string>

#ifndef HOMEWORK_HW1_NBAPLAYER_H_
#define HOMEWORK_HW1_NBAPLAYER_H_

/**
 * Class to handle creation and addition of NBA player data. 
 */
class Baller {
 public:
        Baller();
        Baller(std::string, std::string, char, std::string);

        std::string school() const;
        std::string league() const;
        std::string name() const;
        char position() const;
        std::string team() const;

        void set_school(std::string school);
        void set_name(std::string name);
        void set_position(char position);
        void set_team(std::string team);
        void display_baller();

 private:
        std::string school_, league_, name_, team_;
        char position_;
};
#endif  // HOMEWORK_HW1_NBAPLAYER_H_
