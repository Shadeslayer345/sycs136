#include <cstddef>
#include <fstream>
#include <iostream>
#include <string>

/**
 * Specification
 */

class studentType {
 public:
    studentType();

    void print();

    std::string first_name, last_name, city;
    int team_id;
};

class nodeType {
 public:
    nodeType();
    ~nodeType();

    studentType info;
    nodeType * next;
};

class unsortedListType {
 public:
    unsortedListType();
    ~unsortedListType();

    void insertMid(studentType & new_student);
    void insertEnd(studentType & new_student);
    void insertFront(studentType & new_student);
    void printList();

    nodeType * firstPtr;
    int length;
};

/**
 * Implementation
 */

 studentType::studentType() {
     first_name = "";
     last_name = "";
     team_id = 0;
     city = "";
 }

 void studentType::print() {
     std::cout << first_name << " " << last_name << "  " << team_id
            << " " << city << std::endl;
 }

 nodeType::nodeType() {
    this->next = NULL;
    this->info.first_name = "";
    this->info.last_name = "";
    this->info.team_id = 0;
    this->info.city = "";
 }

 nodeType::~nodeType() {
     delete next;
  }

 unsortedListType::unsortedListType() {
     this->firstPtr = NULL;
     length = 0;
  }

 unsortedListType::~unsortedListType() {
     nodeType * temp = new nodeType();
     while (temp->next != NULL) {
         temp = firstPtr->next;
         delete firstPtr;
         firstPtr = temp;
     }
  }

  void unsortedListType::insertMid(studentType & new_student) {
     nodeType * traverse = new nodeType();
     traverse = firstPtr;
     nodeType * new_node = new nodeType();
     new_node->info = new_student;
     for (int i = 0; i < (length / 2) - 1; ++i) {
         traverse = traverse->next;
     }
     new_node->next = traverse->next;
     traverse->next = new_node;
     length++;
  }

  void unsortedListType::insertEnd(studentType & new_student) {
     nodeType * new_node = new nodeType();
     new_node->info = new_student;
     if (firstPtr == NULL) {
         firstPtr = new_node;
     } else {
         nodeType * traverse = new nodeType();
         traverse = firstPtr;
         while (traverse->next != NULL) {
             traverse = traverse->next;
         }
         traverse->next = new_node;
     }
     length++;
  }

  void unsortedListType::insertFront(studentType & new_student) {
     nodeType * new_node = new nodeType();
     new_node->info = new_student;
     if (firstPtr == NULL) {
         firstPtr = new_node;
     } else {
         new_node->next = firstPtr;
         firstPtr = new_node;
     }
     length++;
  }

 void unsortedListType::printList() {
     if (firstPtr == NULL) {
         std::cout << "Empty list" << std::endl << std::endl;
     } else if (firstPtr->next == NULL) {
         firstPtr->info.print();
     } else {
         nodeType * traverse = new nodeType();
         traverse = firstPtr;
         while (traverse != NULL) {
             traverse->info.print();
             traverse = traverse->next;
         }
         delete traverse;
     }
 }

/**
 * Driver
 */

int main() {
    std::ifstream myfile;
    myfile.open("infile.txt");
    std::string student_first_name, student_last_name, student_city;
    int student_team_id;
    unsortedListType studentList;
    for (int i = 0; i < 35; ++i) {
        myfile >> student_first_name >> student_last_name >> student_team_id
                >> student_city;
        studentType temp_student;
        temp_student.first_name = student_first_name;
        temp_student.last_name = student_last_name;
        temp_student.team_id = student_team_id;
        temp_student.city = student_city;

        if ((student_team_id % 2) == 0) {
            studentList.insertFront(temp_student);
        } else {
            studentList.insertEnd(temp_student);
        }
    }

    studentType final_student;
    final_student.first_name = "Harry";
    final_student.last_name = "Keeling";
    final_student.team_id = 20;
    final_student.city = "Dallas";

    studentList.insertMid(final_student);

    studentList.printList();


    return 0;
}

/**
johan lingani  8 Woodbridge
Terrance Ellis  6 Columbia
Candace Ross  0 Rockville
victor foreman  4 Earlsville
Malcolm Williams  2 Winchester
Illium Williams  8 Juray
Ranjay Salmon  2 Trenton
Kareem Parris-Baptiste  8 Princeton
Blanche Mahop  6 Austin
crepin mahop  2 Bolder
Matthew Jacobs  8 Nashville
Allee Clark  4 Pittsburg
Charles Brown  0 Bowie
Dhuel Fisher  8 Richmord
Alyssa Bullock  6 Baltimore
Adegboyega Akinsiku  4 Topeka
Remington Holt  2 Detriot
Harry Keeling  20 Dallas
Kendall Keeling  3 Miami
Joanna Phillip  1 Tampa
jonnetta bratcher  3 Charleston
Kerisha Burke  7 Washington
Brionna Huskey  1 Sacramento
Brittany Jackson  5 Denver
Kourtnei Langley  9 Louisville
Ashlee McKinney  3 Houston
Jennifer Ojie  7 Arlington
Michael Phillips  3 Boston
Aja Walton  3 Boise
Kyle Ward  5 Germantown
Saboor Salaam  1 Reston
Cherith-Eden Clements  5 Hagerstown
Isa Edwards-El  3 Greensburg
Jaleesa Harrigan  1 Harrisonburg
Susan Angus  3 Burke
Rashad Rose  7 Fredricksburg
 */
