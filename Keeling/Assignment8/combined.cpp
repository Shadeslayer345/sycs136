#include <cstddef>
#include <fstream>
#include <iostream>
#include <string>

#ifndef KEELING_ASSIGNMENT8_NODETYPE_H
#define KEELING_ASSIGNMENT8_NODETYPE_H
class nodeType {
 public:
    nodeType();
    ~nodeType();
    std::string info;
    nodeType * next;
};
#endif

#ifndef KEELING_ASSIGNMENT8_QUETYPE_H
#define KEELING_ASSIGNMENT8_QUETYPE_H
class queType {
 public:
    queType();
    ~queType();

    void enqueue(std::string item);
    void dequeue();
 private:
    nodeType * front_;
    nodeType * rear_;
};
#endif

nodeType::nodeType() {
    this->info = "";
    this->next = NULL;
}

nodeType::~nodeType() {
    delete next;
}

queType::queType() {
    this->front_ = NULL;
    this->rear_ = NULL;
}

queType::~queType() {
    while (front_ && rear_ != NULL) {
        if (front_->next == NULL) {
            delete front_;
            front_ = NULL;
            rear_ = NULL;
        } else {
            front_ = front_->next;
        }
    }
}

void queType::enqueue(std::string item) {
    nodeType * temp = new nodeType();
    temp->info = item;
    if (rear_ == NULL) {
        rear_ = temp;
        front_ = rear_;
    } else {
        rear_->next = temp;
        rear_ = rear_->next;
    }
}

void queType::dequeue() {
    if (front_ == NULL) {
        std::cout << "Can't dequeue from empty queue" << std::endl;
    } else {
        std::cout << front_->info << std::endl;
        if (front_->next == NULL) {
            delete front_;
            front_ = NULL;
            rear_ = NULL;
        } else {
            front_ = front_->next;
        }
    }
}

int main() {
    std::ifstream infile;
    infile.open("infile.txt");
    queType que1;
    for (int i = 0; i < 6; ++i) {
        std::string temp = "";
        infile >> temp;
        que1.enqueue(temp);
    }

    for (int i = 0; i < 3; ++i) {
        que1.dequeue();
    }

    return 0;
}

/**
Denver
Houston
Washington
 */
