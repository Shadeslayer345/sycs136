#include "studentType.h"

studentType::studentType() {
    first_name = "";
    last_name = "";
    team_id = 0;
    city = "";
}

void studentType::print() {
    std::cout << first_name << " " << last_name << "    " << team_id << "   "
            << city << std::endl;
}
