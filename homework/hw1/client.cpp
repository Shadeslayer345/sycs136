/** Copyright 2014 Barry C. Harris Jr. */

#include <stdio.h>
#include <string>

#include "NBAplayer.h"
#include "NFLplayer.h"

/**
 * The main client interface code. Will take the user's input and create either 
 * an instance of the NBAplayer class or a NFLplayer struc and set the 
 * respective objects data members.
 */

/** Overloaded functions for both NFL and NBA players */
void create_player();
void set_player_name(Baller& player);
void set_player_current_team(Baller& player);
void set_player_position(Baller& player);
void set_player_school(Baller& player);
void confirmation(Baller& player);

void create_player();
void set_player_name(Shotcaller& player);
void set_player_current_team(Shotcaller& player);
void set_player_position(Shotcaller& player);
void set_player_school(Shotcaller& player);
void confirmation(Shotcaller& player);

int main() {
    create_player();
    return 0;
}  // end main()

/**
 * Main code that handles the interface and function calls
 */
void create_player() {
    std::cout << "Hello!" << std::endl << "Welcome to the SportsFan, Inc. " <<
                "player tool!" << std::endl;
    std::cout << "Would you like to create a NBA(1) or NFL player(2)? ";

    int sport = 0;
    std::cin >> sport;

    std::cout << "You choose a " << ((sport == 1) ? "NBA" : "NFL") << " player!"
                << std::endl;

    if (sport == 1) {
        Baller player;
        set_player_name(player);
        set_player_current_team(player);
        set_player_position(player);
        set_player_school(player);
        confirmation(player);
    } else if (sport == 2) {
        Shotcaller player;
        set_player_name(player);
        set_player_current_team(player);
        set_player_position(player);
        set_player_school(player);
        confirmation(player);
    }
}  // end create player

/**
 * Sets the name for a Baller.
 * @param player Instance of the Baller class.
 */
void set_player_name(Baller& player) {
    std::string player_name = "";

    std::cout << "Now what is this player's name? ";

    std::cin.get();
    getline(std::cin, player_name);

    player.set_name(player_name);
}  // end set player name

/**
 * Sets the name for a Shotcaller.
 * @param player Instance of the Shotcaller class.
 */
void set_player_name(Shotcaller& player) {
    std::string player_name = "";

    std::cout << "Now what is this player's name? ";

    std::cin.get();
    getline(std::cin, player_name);

    player.name = player_name;
}  // end set player name

/**
 * Sets the team for a Baller.
 * @param player Instance of the Baller class.
 */
void set_player_current_team(Baller& player) {
    std::string current_team = "";

    std::cout << "What team in the " << player.league() << " does " <<
                player.name() << " play for? ";

    getline(std::cin, current_team);

    player.set_team(current_team);
}  // end set player current team

/**
 * Sets the team for a Shotcaller.
 * @param player Instance of the Shotcaller class.
 */
void set_player_current_team(Shotcaller& player) {
    std::string current_team = "";

    std::cout << "What team in the " << player.league << " does " << player.name
                << " play for? ";

    getline(std::cin, current_team);

    player.team = current_team;
}  // end set player current team

/**
 * Sets the position for a Baller.
 * @param player Instance of the Baller class.
 */
void set_player_position(Baller& player) {
    char position = '\0';

    std::cout << "What position does " << player.name() << " play on the " <<
                player.team() << "? ";

    getchar();
    position = getchar();

    player.set_position(position);
}  // end set player position

/**
 * Sets the position for the Shotcaller class.
 * @param player Instance of the Shotcaller class.
 */
void set_player_position(Shotcaller& player) {
    std::string position = "";

    std::cout << "What position does " << player.name << " play on the " <<
                player.team << "? ";

    getline(std::cin, position);

    player.position = position;
}  // end set player position

/**
 * Sets the school for the Baller class.
 * @param  player [description]
 * @return        [description]
 */
void  set_player_school(Baller& player) {
    std::string school = "";

    std::cout << "And finally, what school did " << player.name() << " attend "
                "before entering the " << player.league() << "? ";

    getline(std::cin, school);

    player.set_school(school);
}  // end set player alma mater

void  set_player_school(Shotcaller& player) {
    std::string school = "";

    std::cout << "And finally, what school did " << player.name <<
                " attend before entering the " << player.league << "? ";

    getline(std::cin, school);

    player.school = school;
}  // end set player alma mater

void confirmation(Baller& player) {
    std::cout << "Okay does this look correct? " << std::endl;
    player.display_baller();
    std::cout << "(y/n) ";

    char finished = getchar();

    if (finished == 'y') {
        std::cout << "Thank you for creating a player!" << std::endl <<
                    "Good bye!" << std::endl;
    } else {
        std::cout << "Sorry, please run the program again to create a new " <<
                    "player!!" << std::endl;
    }
}  // end confirmation

void confirmation(Shotcaller& player) {
    std::cout << "Okay does this look correct? " << std::endl;
    std::cout << "League: NFL" << std::endl <<
            "(1) Name: " << player.name << std::endl <<
            "(2) Current Team: " << player.team << std::endl <<
            "(3) Position: " << player.position << std::endl <<
            "(4) School: " << player.school << std::endl;
    std::cout << "(y/n) ";

    char finished = getchar();

    if (finished == 'y') {
        std::cout << "Thank you for creating a player!" << std::endl <<
                    "Goodbye!" << std::endl;
    } else {
        std::cout << "Sorry, please run the program again to create a new " <<
                    "player!!" << std::endl;
    }
}  // end confirmation


