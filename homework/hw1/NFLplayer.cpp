/** Copyright 2014 Barry C. Harris Jr. */

#include "NFLplayer.h"

/**
 * Default constructor for an NFL player which sets it's league data member.
 * @constructor
 */
Shotcaller::Shotcaller() {
    league = "NFL";
}
