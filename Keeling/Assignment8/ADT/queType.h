#include <cstddef>
#include <iostream>
#include <string>

#ifndef KEELING_ASSIGNMENT8_ADT_NODETYPE_H
#define KEELING_ASSIGNMENT8_ADT_NODETYPE_H
class nodeType {
 public:
    nodeType();
    ~nodeType();
    std::string info;
    nodeType * next;
};
#endif

#ifndef KEELING_ASSIGNMENT8_ADT_QUETYPE_H
#define KEELING_ASSIGNMENT8_ADT_QUETYPE_H
class queType {
 public:
    queType();
    ~queType();

    void enqueue(std::string item);
    void dequeue();
 private:
    nodeType * front_;
    nodeType * rear_;
};
#endif
