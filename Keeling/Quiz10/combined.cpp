#include <fstream>
#include <iostream>
#include <string>

class NodeType {
 public:
    std::string info;
    NodeType * next;
};

class StackType {
 public:
  StackType(); // Constructor
  ~StackType(); // Destructor
  void Push(std::string newWord); //Pushes string parameter onto top of stack
  std::string Pop(); //deletes the top node and returns string in node
  NodeType * topPtr;
};

StackType::StackType() {
    topPtr = NULL;
}

StackType::~StackType() {
    while (topPtr != NULL) {
        Pop();
    }
}

std::string StackType::Pop() {
    if (topPtr == NULL) {
        return "STACK EMPTY!";
    } else {
        std::string removedInfo = topPtr->info;
        NodeType* temp = new NodeType();
        temp = topPtr;
        topPtr = topPtr->next;
        delete temp;
        return removedInfo;
    }
}

void StackType::Push(std::string newWord ) {
    NodeType * newNode;
    newNode = new NodeType();
    newNode->info = newWord;
    newNode->next = topPtr;
    topPtr = newNode;
}

int main() {
    StackType cityStack;
    std::ifstream infile;
    infile.open("infile.txt");
    std::string tempCity = "";
    infile >> tempCity;
    while (infile) {
        cityStack.Push(tempCity);
        infile >> tempCity;
    }
    for (int i = 0; i < 3; ++i) {
        std::cout << cityStack.Pop() << std::endl;
    }

    return 0;
}

/**
Dallas
Chicago
Austin
 */
