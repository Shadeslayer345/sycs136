#include "MoneyType.h"

MoneyType::MoneyType() {
    dollars_ = 0;
    cents_ = 0;
}

MoneyType::MoneyType(long dollars, long cents) {
    dollars_ = dollars;
    cents_ = cents;
}

long MoneyType::DollarsAre() const {
    return dollars_;
}

long MoneyType::CentsAre() const {
    return cents_;
}
